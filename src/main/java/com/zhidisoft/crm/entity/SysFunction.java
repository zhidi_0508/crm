package com.zhidisoft.crm.entity;

import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

public class SysFunction extends BaseEntity {

	private static final long serialVersionUID = -2956175021825453787L;
	@NotBlank(message="编号不能为空")
	private String func_code;
	@NotBlank(message="名称不能为空")
	private String func_name;
	private Integer func_type;
	private String func_url;
	private String parent_id;
	private Date update_time;
	
	public String getFunc_code() {
		return func_code;
	}
	public void setFunc_code(String func_code) {
		this.func_code = func_code;
	}
	public String getFunc_name() {
		return func_name;
	}
	public void setFunc_name(String func_name) {
		this.func_name = func_name;
	}
	public Integer getFunc_type() {
		return func_type;
	}
	public void setFunc_type(Integer func_type) {
		this.func_type = func_type;
	}
	public String getFunc_url() {
		return func_url;
	}
	public void setFunc_url(String func_url) {
		this.func_url = func_url;
	}
	public String getParent_id() {
		return parent_id;
	}
	public void setParent_id(String parent_id) {
		this.parent_id = parent_id;
	}
	public Date getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}
	@Override
	public String toString() {
		return "SysFunction [func_code=" + func_code + ", func_name=" + func_name + ", func_type=" + func_type
				+ ", func_url=" + func_url + ", parent_id=" + parent_id + ", update_time=" + update_time + "]";
	}
	
}
