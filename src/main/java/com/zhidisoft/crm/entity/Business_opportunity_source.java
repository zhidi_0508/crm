package com.zhidisoft.crm.entity;

public class Business_opportunity_source extends BaseEntity{

	private static final long serialVersionUID = 1L;
	private String source_name;

	public String getSource_name() {
		return source_name;
	}

	public void setSource_name(String source_name) {
		this.source_name = source_name;
	}
	
}
