package com.zhidisoft.crm.entity;

import java.util.Date;

public class BusinessBoost extends BaseEntity {

	private static final long serialVersionUID = 7330018138685491549L;

	private String BUSINESS_STATUS_ID;
	private Date NEXT_CONTACT_TIME;
	private byte[] NEXT_CONTACT_INFO;
	private byte[] DESCRIPTION;
	private String USER_ID;
	
	public String getUSER_ID() {
		return USER_ID;
	}
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}
	public String getBUSINESS_STATUS_ID() {
		return BUSINESS_STATUS_ID;
	}
	public void setBUSINESS_STATUS_ID(String bUSINESS_STATUS_ID) {
		BUSINESS_STATUS_ID = bUSINESS_STATUS_ID;
	}
	public Date getNEXT_CONTACT_TIME() {
		return NEXT_CONTACT_TIME;
	}
	public void setNEXT_CONTACT_TIME(Date nEXT_CONTACT_TIME) {
		NEXT_CONTACT_TIME = nEXT_CONTACT_TIME;
	}
	public byte[] getNEXT_CONTACT_INFO() {
		return NEXT_CONTACT_INFO;
	}
	public void setNEXT_CONTACT_INFO(byte[] nEXT_CONTACT_INFO) {
		NEXT_CONTACT_INFO = nEXT_CONTACT_INFO;
	}
	public byte[] getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(byte[] dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
	
}
