package com.zhidisoft.crm.entity;

import java.util.Date;

public class BusinessRecycleBin extends BaseEntity {

	private static final long serialVersionUID = 1309280982665733727L;
	private String charge_user_id;
	private String customer_id;
	private Integer business_money;
	private String business_name;
	private String contact_name;
	private String contract_address;
	private String business_type_id;
	private String business_status_id;
	private String business_source_id;
	private Integer win_rate;
	private Integer expected_transaction_price;
	private Date next_contact_time;
	private String next_contact_info;//下次联系内容
	private byte[] remark;
	private Date create_time;//创建时间
	private String create_user_id;//创建人
	private Date update_time;
	private String update_user_id;
	private String create_name;
	private String sremovestate;
	
	public String getSremovestate() {
		return sremovestate;
	}
	public void setSremovestate(String sremovestate) {
		this.sremovestate = sremovestate;
	}
	public String getCreate_user_id() {
		return create_user_id;
	}
	public void setCreate_user_id(String create_user_id) {
		this.create_user_id = create_user_id;
	}
	public String getCreate_name() {
		return create_name;
	}
	public void setCreate_name(String create_name) {
		this.create_name = create_name;
	}
	public Date getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}
	public String getUpdate_user_id() {
		return update_user_id;
	}
	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}
	public String getCharge_user_id() {
		return charge_user_id;
	}
	public void setCharge_user_id(String charge_user_id) {
		this.charge_user_id = charge_user_id;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public String getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}
	
	public Integer getBusiness_money() {
		return business_money;
	}
	public void setBusiness_money(Integer business_money) {
		this.business_money = business_money;
	}
	public String getBusiness_name() {
		return business_name;
	}
	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}
	public String getContact_name() {
		return contact_name;
	}
	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}
	public String getContract_address() {
		return contract_address;
	}
	public void setContract_address(String contract_address) {
		this.contract_address = contract_address;
	}
	public String getBusiness_type_id() {
		return business_type_id;
	}
	public void setBusiness_type_id(String business_type_id) {
		this.business_type_id = business_type_id;
	}
	public String getBusiness_status_id() {
		return business_status_id;
	}
	public void setBusiness_status_id(String business_status_id) {
		this.business_status_id = business_status_id;
	}
	public String getBusiness_source_id() {
		return business_source_id;
	}
	public void setBusiness_source_id(String business_source_id) {
		this.business_source_id = business_source_id;
	}
	public Integer getWin_rate() {
		return win_rate;
	}
	public void setWin_rate(Integer integer) {
		this.win_rate = integer;
	}
	public Integer getExpected_transaction_price() {
		return expected_transaction_price;
	}
	public void setExpected_transaction_price(Integer integer) {
		this.expected_transaction_price = integer;
	}
	public Date getNext_contact_time() {
		return next_contact_time;
	}
	public void setNext_contact_time(Date next_contact_time) {
		this.next_contact_time = next_contact_time;
	}
	public String getNext_contact_info() {
		return next_contact_info;
	}
	public void setNext_contact_info(String next_contact_info) {
		this.next_contact_info = next_contact_info;
	}
	public byte[] getRemark() {
		return remark;
	}
	public void setRemark(byte[] remark) {
		this.remark = remark;
	}

}
