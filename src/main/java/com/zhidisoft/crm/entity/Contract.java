package com.zhidisoft.crm.entity;

import java.util.Date;

public class Contract extends BaseEntity {

	private static final long serialVersionUID = 6286419619618339387L;
	private String CONTRACT_NO;//合同编辑
	private String CUSTOMER_ID;//客户ID(来源客户)
	private String CONTACT_NAME;//联系人
	private String CHARGE_USER_ID;//负责人
	private Date DATE_OF_CONTRACT;//签约日期
	private Integer CONTRACT_MONEY;//合同金额
	private String CONTRACT_STATUS_ID;//状态
	private String BUSINESS_OPPORTUNITY_ID;//来源商机
	private Date EFFECTIVE_DATE_OF_CONTRACT;//合同生效日期
	private Date CONTRACT_EXPIRY_DATE;//合同到期日期
	private byte[] TERMS_AND_CONDITIONS;//条件和条款
	private	String THE_CONTRACT_DESCRIPTION;//合同描述
	
	public String getBUSINESS_OPPORTUNITY_ID() {
		return BUSINESS_OPPORTUNITY_ID;
	}
	public void setBUSINESS_OPPORTUNITY_ID(String bUSINESS_OPPORTUNITY_ID) {
		BUSINESS_OPPORTUNITY_ID = bUSINESS_OPPORTUNITY_ID;
	}
	public String getCONTRACT_NO() {
		return CONTRACT_NO;
	}

	public void setCONTRACT_NO(String cONTRACT_NO) {
		CONTRACT_NO = cONTRACT_NO;
	}

	public String getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}

	public void setCUSTOMER_ID(String cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}

	public String getCONTACT_NAME() {
		return CONTACT_NAME;
	}

	public void setCONTACT_NAME(String cONTACT_NAME) {
		CONTACT_NAME = cONTACT_NAME;
	}

	public String getCHARGE_USER_ID() {
		return CHARGE_USER_ID;
	}

	public void setCHARGE_USER_ID(String cHARGE_USER_ID) {
		CHARGE_USER_ID = cHARGE_USER_ID;
	}

	public Date getDATE_OF_CONTRACT() {
		return DATE_OF_CONTRACT;
	}

	public void setDATE_OF_CONTRACT(Date dATE_OF_CONTRACT) {
		DATE_OF_CONTRACT = dATE_OF_CONTRACT;
	}

	public Integer getCONTRACT_MONEY() {
		return CONTRACT_MONEY;
	}

	public void setCONTRACT_MONEY(Integer cONTRACT_MONEY) {
		CONTRACT_MONEY = cONTRACT_MONEY;
	}

	public String getCONTRACT_STATUS_ID() {
		return CONTRACT_STATUS_ID;
	}

	public void setCONTRACT_STATUS_ID(String cONTRACT_STATUS_ID) {
		CONTRACT_STATUS_ID = cONTRACT_STATUS_ID;
	}

	public Date getEFFECTIVE_DATE_OF_CONTRACT() {
		return EFFECTIVE_DATE_OF_CONTRACT;
	}

	public void setEFFECTIVE_DATE_OF_CONTRACT(Date eFFECTIVE_DATE_OF_CONTRACT) {
		EFFECTIVE_DATE_OF_CONTRACT = eFFECTIVE_DATE_OF_CONTRACT;
	}

	public Date getCONTRACT_EXPIRY_DATE() {
		return CONTRACT_EXPIRY_DATE;
	}

	public void setCONTRACT_EXPIRY_DATE(Date cONTRACT_EXPIRY_DATE) {
		CONTRACT_EXPIRY_DATE = cONTRACT_EXPIRY_DATE;
	}

	public byte[] getTERMS_AND_CONDITIONS() {
		return TERMS_AND_CONDITIONS;
	}

	public void setTERMS_AND_CONDITIONS(byte[] tERMS_AND_CONDITIONS) {
		TERMS_AND_CONDITIONS = tERMS_AND_CONDITIONS;
	}

	public String getTHE_CONTRACT_DESCRIPTION() {
		return THE_CONTRACT_DESCRIPTION;
	}

	public void setTHE_CONTRACT_DESCRIPTION(String tHE_CONTRACT_DESCRIPTION) {
		THE_CONTRACT_DESCRIPTION = tHE_CONTRACT_DESCRIPTION;
	}
}
