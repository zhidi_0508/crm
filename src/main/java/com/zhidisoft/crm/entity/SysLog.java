package com.zhidisoft.crm.entity;

import java.util.Date;

public class SysLog extends BaseEntity {
	private static final long serialVersionUID = -246617954986104267L;
	private String method_name;
	private String method_info;
	private Date create_time;
	private String user_id;
	
	public String getMethod_name() {
		return method_name;
	}
	public void setMethod_name(String method_name) {
		this.method_name = method_name;
	}
	public String getMethod_info() {
		return method_info;
	}
	public void setMethod_info(String method_info) {
		this.method_info = method_info;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	@Override
	public String toString() {
		return "ServiceLog [method_name=" + method_name + ", method_info=" + method_info + ", create_time="
				+ create_time + ", user_id=" + user_id + "]";
	}
	
}
