package com.zhidisoft.crm.entity;

import org.hibernate.validator.constraints.NotBlank;

/*
 * 角色管理
 */
public class SysRole extends BaseEntity{

	private static final long serialVersionUID = 1L;

	@NotBlank(message="角色名字不能为空")
	private String role_name;
	@NotBlank(message="角色内容不能为空")
	private String role_info;


	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	public String getRole_info() {
		return role_info;
	}

	public void setRole_info(String role_info) {
		this.role_info = role_info;
	}

	@Override
	public String toString() {
		return "SysRole [ role_name=" + role_name + ", role_info=" + role_info + "]";
	}

}
