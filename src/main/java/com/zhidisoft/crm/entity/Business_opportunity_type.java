package com.zhidisoft.crm.entity;

public class Business_opportunity_type extends BaseEntity{

	private static final long serialVersionUID = 1L;
	private String type_name;

	public String getType_name() {
		return type_name;
	}

	public void setType_name(String type_name) {
		this.type_name = type_name;
	}
	
}
