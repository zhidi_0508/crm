package com.zhidisoft.crm.entity;

public class Customer extends BaseEntity {

	private static final long serialVersionUID = 3406690922758052201L;

	private String customer_name;

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	
}
