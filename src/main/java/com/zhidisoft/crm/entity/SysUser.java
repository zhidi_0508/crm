package com.zhidisoft.crm.entity;

import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;

public class SysUser extends BaseEntity {

	private static final long serialVersionUID = 1L;
	@NotBlank(message="用户名不能为空")
	private String username;
	@NotBlank(message="密码不能为空")
	private String password;
	private Date create_time;
	private Integer is_admin;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Integer getIs_admin() {
		return is_admin;
	}

	public void setIs_admin(Integer is_admin) {
		this.is_admin = is_admin;
	}

	@Override
	public String toString() {
		return "SysUser [username=" + username + ", password=" + password + ", create_time=" + create_time
				+ ", is_admin=" + is_admin + "]";
	}

}
