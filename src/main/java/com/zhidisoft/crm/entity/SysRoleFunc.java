package com.zhidisoft.crm.entity;

import java.util.Date;

public class SysRoleFunc extends BaseEntity {

	private static final long serialVersionUID = 3717168363324747710L;
	private String role_id;
	private String func_id;
	private Date create_time;
	public String getRole_id() {
		return role_id;
	}
	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}
	public String getFunc_id() {
		return func_id;
	}
	public void setFunc_id(String func_id) {
		this.func_id = func_id;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	
	@Override
	public String toString() {
		return "SysRoleFunc [role_id=" + role_id + ", func_id=" + func_id + "]";
	}
}
