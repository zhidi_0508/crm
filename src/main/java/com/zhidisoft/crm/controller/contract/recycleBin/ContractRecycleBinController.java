package com.zhidisoft.crm.controller.contract.recycleBin;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.service.IBusinessOpportunityService;
import com.zhidisoft.crm.service.IBussinessService;
import com.zhidisoft.crm.service.IContractRecycleService;
import com.zhidisoft.crm.service.ISysUserService;
import com.zhidisoft.crm.util.DateUtil;
import com.zhidisoft.crm.vo.ContractListVo;
import com.zhidisoft.crm.vo.PageVo;

@Controller
@RequestMapping("/contract/recycleBin")
public class ContractRecycleBinController extends BaseController {

	@Autowired
	IContractRecycleService contractService;

	@Autowired
	IBussinessService bService;

	@Autowired
	ISysUserService userService;

	@Autowired
	IBusinessOpportunityService bopportunityService;

	@DeleteMapping("/recover")
	@ResponseBody
	public void deleteAll(@RequestBody String ids) {
		String[] idArr = ids.split(",");// 字符串转换为数组
		for (String id : idArr) {
			contractService.recoverAll(id);
		}
	}

	/**
	 * 合同分页列表(高级查询)
	 * @param pageNum
	 * @param pageSize
	 * @param charge_username
	 * @return
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "CriteriaType", defaultValue = "-1") String CriteriaType,
			@RequestParam(name = "content", defaultValue = "") String content) throws IOException {
		content = new String(content.getBytes("ISO8859-1"), "UTF-8");
		if(CriteriaType=="-1"){
			content = "";
		}
		PageVo pageVo = contractService.findPage(pageNum, pageSize,CriteriaType,content);
		ModelAndView mav = new ModelAndView("contract/recycle_bin/list.html", "list", pageVo);
		mav.addObject("CriteriaType", CriteriaType);
		mav.addObject("content", content);
		return mav;
	}
	
	/**
	 * 
	 * 查看详情
	 * @param id
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/{id}")
	public ModelAndView detailUi(@PathVariable("id") String id) throws IOException {
		ContractListVo vo = contractService.findByid(id);
		ModelAndView mav = new ModelAndView("contract/recycle_bin/detail.html", "contract", vo);
		mav.addObject("terms_and_conditions", new String(vo.getTerms_and_conditions(), "utf-8"));
		return mav;
	}

	/*
	 * 导出
	 * 
	 * @return
	 * 
	 * @throws IOException
	 */
	@SuppressWarnings({ "resource", "unchecked" })
	@GetMapping("/export")
	public ResponseEntity<byte[]> exportData() throws IOException {
		// 生成文件名
		StringBuffer fileName = new StringBuffer(session.getServletContext().getRealPath("/")).append("upload")
				.append(File.separatorChar).append("tmp").append(File.separatorChar).append("合同管理-")
				.append(DateUtil.parseStr(new Date(), "yyyy年MM月dd日")).append(".xls");
		// 创建对Excel工作簿文件的引用
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 在Excel工作簿中建一工作表，其名为缺省值
		// 如要新建一名为"合同列表"的工作表，其语句为：
		HSSFSheet sheet = workbook.createSheet("合同列表");

		// 查询数据
		List<ContractListVo> contractList = contractService.findPage(1, 10,null,null).getList();

		// 0行是菜单
		HSSFRow row1 = sheet.createRow((short) 0);
		HSSFCell cell1 = row1.createCell((short) 1);
		cell1.setCellValue("序号");
		HSSFCell cell2 = row1.createCell((short) 2);
		cell2.setCellValue("合同编号");
		HSSFCell cell3 = row1.createCell((short) 3);
		cell3.setCellValue("客户");
		HSSFCell cell4 = row1.createCell((short) 4);
		cell4.setCellValue("联系人");
		HSSFCell cell5 = row1.createCell((short) 5);
		cell5.setCellValue("负责人");
		HSSFCell cell6 = row1.createCell((short) 6);
		cell6.setCellValue("签约日期");
		HSSFCell cell7 = row1.createCell((short) 7);
		cell7.setCellValue("合同金额(元)");
		HSSFCell cell8 = row1.createCell((short) 8);
		cell8.setCellValue("状态");
		HSSFCell cell9 = row1.createCell((short) 9);
		cell9.setCellValue("距合同到期天数");

		int i = 1;
		for (ContractListVo item : contractList) {
			HSSFRow row = sheet.createRow((short) i);
			cell1 = row.createCell((short) 1);
			cell1.setCellValue(i);
			cell2 = row.createCell((short) 2);
			cell2.setCellValue(item.getContract_no());
			cell3 = row.createCell((short) 3);
			cell3.setCellValue(item.getCustomer_name());
			cell4 = row.createCell((short) 4);
			cell4.setCellValue(item.getContact_name());
			cell5 = row.createCell((short) 5);
			cell5.setCellValue(item.getUsername());
			cell6 = row.createCell((short) 6);
			cell6.setCellValue(DateUtil.parseStr(item.getDate_of_contract(), "yyyy年MM月dd日"));
			cell7 = row.createCell((short) 7);
			cell7.setCellValue(item.getContract_money());
			cell8 = row.createCell((short) 8);
			cell8.setCellValue(item.getStatus_name());
			cell9 = row.createCell((short) 9);
			cell9.setCellValue(item.getEnd_time());
			i++;
		}

		// 新建一输出文件流
		File file = new File(fileName.toString());
		if (!file.exists())
			file.createNewFile();
		workbook.write(file);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDispositionFormData("attachment", file.getName(), Charset.forName("UTF-8"));// 防止文件名乱码
		HttpStatus statusCode = HttpStatus.OK;
		ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers,
				statusCode);
		return entity;
	}

}
