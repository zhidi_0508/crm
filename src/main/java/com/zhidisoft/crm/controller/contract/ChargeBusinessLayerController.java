package com.zhidisoft.crm.controller.contract;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.zhidisoft.crm.service.IBusinessOpportunityService;

@Controller
@RequestMapping("/contract/business/layer")
public class ChargeBusinessLayerController {

	@Autowired
	IBusinessOpportunityService businessService;
	
	@GetMapping
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "CriteriaType", defaultValue = "-1") String CriteriaType,
			@RequestParam(name = "content", defaultValue = "") String content) throws IOException {
		content = new String(content.getBytes("ISO8859-1"), "UTF-8");
		ModelAndView mav = new ModelAndView("/contract/business/layer.html", "list",
				businessService.findPage(pageNum, pageSize, CriteriaType,content));
		return mav; // 省代码(视图解析器)
	}

}
