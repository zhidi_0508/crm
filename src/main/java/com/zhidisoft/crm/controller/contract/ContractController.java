package com.zhidisoft.crm.controller.contract;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.entity.Contract;
import com.zhidisoft.crm.entity.Contract_status;
import com.zhidisoft.crm.entity.Customer;
import com.zhidisoft.crm.entity.SysUser;
import com.zhidisoft.crm.service.IBusinessOpportunityService;
import com.zhidisoft.crm.service.IBussinessService;
import com.zhidisoft.crm.service.IContractService;
import com.zhidisoft.crm.service.ISysUserService;
import com.zhidisoft.crm.util.DateUtil;
import com.zhidisoft.crm.vo.BusinessOpportunityVo;
import com.zhidisoft.crm.vo.ContractCountVo;
import com.zhidisoft.crm.vo.ContractListVo;
import com.zhidisoft.crm.vo.ContractVo;
import com.zhidisoft.crm.vo.PageVo;

@Controller
@RequestMapping("/contract")
public class ContractController extends BaseController {

	@Autowired
	IContractService contractService;

	@Autowired
	IBussinessService bService;

	@Autowired
	ISysUserService userService;

	@Autowired
	IBusinessOpportunityService bopportunityService;
	
	/**
	 * 获取合同一周报表统计
	 * @return
	 */
	@GetMapping("/echarts")
	public ModelAndView echarts() {
		ModelAndView mav = new ModelAndView("contract/echarts.html");
		String firstDayOfWeek = DateUtil.getFirstDayOfWeek().toString();
		String lastDayOfWeek = DateUtil.getLastDayOfWeek().toString();
		String firstDayOfMonth = DateUtil.getFirstDayOfMonth().toString();
		String lastDayOfMonth = DateUtil.getLastDayOfMonth().toString();
		List<ContractCountVo> countData = contractService.contractCount(firstDayOfWeek, lastDayOfWeek);
		mav.addObject("countData", countData);
		List<ContractCountVo> countByMonth = contractService.contractCount(firstDayOfMonth, lastDayOfMonth);
		mav.addObject("countByMonth", countByMonth);
		return mav;
	}

	/**
	 * 点击进入新增页面
	 * 
	 * @return
	 */
	@SuppressWarnings("static-access")
	@GetMapping("/save")
	public ModelAndView saveUi() {
		ModelAndView mav = new ModelAndView("contract/save.html", "customers", bService.findCustomerAll());
		// 合同编号为时间
		mav.addObject("CONTRACT_NO", "crm" + new DateUtil().parseStr(new Date(), "yyyyMMddhhmmssS"));
		mav.addObject("users", userService.findUserAll());
		mav.addObject("status", contractService.findStatusAll());
		return mav;
	}

	/**
	 * 
	 * 添加页面保存按钮
	 */
	@PostMapping("/save")
	public ModelAndView save(@Valid ContractVo contractVo, BindingResult result,
			RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView("redirect:/contract/list");
		if (result.hasErrors()) {
			mav = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mav;
		}
		Contract contract = new Contract();
		contract.setCONTRACT_NO(contractVo.getCONTRACT_NO());
		contract.setCUSTOMER_ID(contractVo.getCUSTOMER_ID());
		contract.setCONTRACT_STATUS_ID(contractVo.getCONTRACT_STATUS_ID());
		contract.setDATE_OF_CONTRACT(DateUtil.toDate(contractVo.getDATE_OF_CONTRACT()));
		contract.setBUSINESS_OPPORTUNITY_ID(contractVo.getBUSINESS_OPPORTUNITY_ID());
		contract.setCONTACT_NAME(contractVo.getCONTACT_NAME());
		contract.setCHARGE_USER_ID(contractVo.getCHARGE_USER_ID());
		contract.setCONTRACT_MONEY(contractVo.getCONTRACT_MONEY());
		contract.setEFFECTIVE_DATE_OF_CONTRACT(DateUtil.toDate(contractVo.getEFFECTIVE_DATE_OF_CONTRACT()));
		contract.setCONTRACT_EXPIRY_DATE(DateUtil.toDate(contractVo.getCONTRACT_EXPIRY_DATE()));
		contract.setTERMS_AND_CONDITIONS(contractVo.getTERMS_AND_CONDITIONS().getBytes());
		contract.setTHE_CONTRACT_DESCRIPTION(contractVo.getTHE_CONTRACT_DESCRIPTION());
		contractService.save(contract);
		return mav;
	}

	/**
	 * 打开编辑页面
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/update/{id}")
	public ModelAndView showUpdate(@PathVariable("id") String id) {

		
		
		Contract contract = contractService.findByContract(id);

		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String DATE_OF_CONTRACT = df.format(contract.getDATE_OF_CONTRACT());
		String CONTRACT_EXPIRY_DATE = df.format(contract.getCONTRACT_EXPIRY_DATE());
		String EFFECTIVE_DATE_OF_CONTRACT = df.format(contract.getEFFECTIVE_DATE_OF_CONTRACT());
		ModelAndView mav = new ModelAndView("contract/update.html", "contract", contract);
		mav.addObject("DATE_OF_CONTRACT", DATE_OF_CONTRACT);
		mav.addObject("CONTRACT_EXPIRY_DATE", CONTRACT_EXPIRY_DATE);
		mav.addObject("EFFECTIVE_DATE_OF_CONTRACT", EFFECTIVE_DATE_OF_CONTRACT);

		List<Customer> list = (List<Customer>) bService.findCustomerAll();
		mav.addObject("customers", list);
		

		String TERMS_AND_CONDITIONS = new String(contract.getTERMS_AND_CONDITIONS());
		mav.addObject("TERMS_AND_CONDITIONS", TERMS_AND_CONDITIONS);
		
		
		List<BusinessOpportunityVo> business = bopportunityService.findBusinessAll();
		mav.addObject("business", business);

		List<SysUser> listuser = (List<SysUser>) userService.findUserAll();
		mav.addObject("users", listuser);

		List<Contract_status> liststatus = contractService.findStatusAll();
		mav.addObject("status", liststatus);

		return mav;
	}

	/**
	 * 提交编辑数据
	 * 
	 * @param contractVo
	 * @return
	 */
	@PostMapping("/update/{id}")
	@ResponseBody
	public ModelAndView Update(@Valid ContractVo contractVo, BindingResult result,
			RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView("redirect:/contract/list");
		if (result.hasErrors()) {
			mav = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mav;
		}
		Contract contract = new Contract();
		contract.setId(contractVo.getId());
		contract.setCONTRACT_NO(contractVo.getCONTRACT_NO());
		contract.setCUSTOMER_ID(contractVo.getCUSTOMER_ID());
		contract.setCONTRACT_STATUS_ID(contractVo.getCONTRACT_STATUS_ID());
		contract.setDATE_OF_CONTRACT(DateUtil.toDate(contractVo.getDATE_OF_CONTRACT()));
		contract.setBUSINESS_OPPORTUNITY_ID(contractVo.getBUSINESS_OPPORTUNITY_ID());
		contract.setCONTACT_NAME(contractVo.getCONTACT_NAME());
		contract.setCHARGE_USER_ID(contractVo.getCHARGE_USER_ID());
		contract.setCONTRACT_MONEY(contractVo.getCONTRACT_MONEY());
		contract.setEFFECTIVE_DATE_OF_CONTRACT(DateUtil.toDate(contractVo.getEFFECTIVE_DATE_OF_CONTRACT()));
		contract.setCONTRACT_EXPIRY_DATE(DateUtil.toDate(contractVo.getCONTRACT_EXPIRY_DATE()));
		contract.setTERMS_AND_CONDITIONS(contractVo.getTERMS_AND_CONDITIONS().getBytes());
		contract.setTHE_CONTRACT_DESCRIPTION(contractVo.getTHE_CONTRACT_DESCRIPTION());
		contractService.update(contract);
		return mav;
	}

	/**
	 * 批量删除
	 * @param ids
	 */
	@DeleteMapping("/delete")
	@ResponseBody
	public void deleteAll(@RequestBody String ids) {
		String[] idArr = ids.split(",");// 字符串转换为数组
		for (String string : idArr) {
			contractService.deleteAll(string);
		}
	}

	/**
	 * 合同分页列表(高级查询)
	 * @param pageNum
	 * @param pageSize
	 * @param charge_username
	 * @return
	 * @throws IOException 
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "CriteriaType", defaultValue = "-1") String CriteriaType,
			@RequestParam(name = "content", defaultValue = "") String content) throws IOException {
		content = new String(content.getBytes("ISO8859-1"), "UTF-8");
		if(CriteriaType=="-1"){
			content = "";
		}
		PageVo pageVo = contractService.findPage(pageNum, pageSize,CriteriaType,content);
		ModelAndView mav = new ModelAndView("contract/list.html", "list", pageVo);
		mav.addObject("CriteriaType", CriteriaType);
		mav.addObject("content", content);
		return mav;
	}

	/**
	 * 
	 * 查看详情
	 * @param id
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/{id}")
	public ModelAndView detailUi(@PathVariable("id") String id) throws IOException {
		ContractListVo vo = contractService.findByid(id);
		ModelAndView mav = new ModelAndView("contract/detail.html", "contract", vo);
		mav.addObject("terms_and_conditions", new String(vo.getTerms_and_conditions(), "utf-8"));
		return mav;
	}

	/*
	 * 导出
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings({ "resource", "unchecked" })
	@GetMapping("/export")
	public ResponseEntity<byte[]> exportData() throws IOException {
		// 生成文件名
		StringBuffer fileName = new StringBuffer(session.getServletContext().getRealPath("/")).append("upload")
				.append(File.separatorChar).append("tmp").append(File.separatorChar).append("合同管理-")
				.append(DateUtil.parseStr(new Date(), "yyyy年MM月dd日")).append(".xls");
		// 创建对Excel工作簿文件的引用
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 在Excel工作簿中建一工作表，其名为缺省值
		// 如要新建一名为"合同列表"的工作表，其语句为：
		HSSFSheet sheet = workbook.createSheet("合同列表");

		// 查询数据
		List<ContractListVo> contractList = contractService.findPage(1, contractService.ContractCount(), "-1","").getList();
		
		// 0行是菜单
		HSSFRow row1 = sheet.createRow((short) 0);
		HSSFCell cell1 = row1.createCell((short) 1);
		cell1.setCellValue("序号");
		HSSFCell cell2 = row1.createCell((short) 2);
		cell2.setCellValue("合同编号");
		HSSFCell cell3 = row1.createCell((short) 3);
		cell3.setCellValue("客户");
		HSSFCell cell4 = row1.createCell((short) 4);
		cell4.setCellValue("联系人");
		HSSFCell cell5 = row1.createCell((short) 5);
		cell5.setCellValue("负责人");
		HSSFCell cell6 = row1.createCell((short) 6);
		cell6.setCellValue("签约日期");
		HSSFCell cell7 = row1.createCell((short) 7);
		cell7.setCellValue("合同金额(元)");
		HSSFCell cell8 = row1.createCell((short) 8);
		cell8.setCellValue("状态");
		HSSFCell cell9 = row1.createCell((short) 9);
		cell9.setCellValue("距合同到期天数");

		int i = 1;
		for (ContractListVo item : contractList) {
			HSSFRow row = sheet.createRow((short) i);
			cell1 = row.createCell((short) 1);
			cell1.setCellValue(i);
			cell2 = row.createCell((short) 2);
			cell2.setCellValue(item.getContract_no());
			cell3 = row.createCell((short) 3);
			cell3.setCellValue(item.getCustomer_name());
			cell4 = row.createCell((short) 4);
			cell4.setCellValue(item.getContact_name());
			cell5 = row.createCell((short) 5);
			cell5.setCellValue(item.getUsername());
			cell6 = row.createCell((short) 6);
			cell6.setCellValue(DateUtil.parseStr(item.getDate_of_contract(), "yyyy年MM月dd日"));
			cell7 = row.createCell((short) 7);
			cell7.setCellValue(item.getContract_money());
			cell8 = row.createCell((short) 8);
			cell8.setCellValue(item.getStatus_name());
			cell9 = row.createCell((short) 9);
			cell9.setCellValue(item.getEnd_time());
			i++;
		}

		// 新建一输出文件流
		File file = new File(fileName.toString());
		if (!file.exists())
			file.createNewFile();
		workbook.write(file);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDispositionFormData("attachment", file.getName(), Charset.forName("UTF-8"));// 防止文件名乱码
		HttpStatus statusCode = HttpStatus.OK;
		ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers,
				statusCode);
		return entity;
	}
}
