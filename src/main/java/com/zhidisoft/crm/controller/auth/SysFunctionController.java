package com.zhidisoft.crm.controller.auth;

import java.io.IOException;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.entity.SysFunction;
import com.zhidisoft.crm.service.ISysFunctionService;
import com.zhidisoft.crm.vo.AjaxVo;

/**
 * auth/user/list get 默认列表(包括查询条件) <br/>
 * auth/user/${id} delete 删除<br/>
 * auth/user get 跳转到新增页面 <br/>
 * auth/user post data 新增<br/>
 * auth/user/${id} get 跳转到修改页面<br/>
 * auth/user/${id} post data 修改
 * 
 * @author MySpace
 *
 */

@Controller
@RequestMapping("/auth/function")
public class SysFunctionController extends BaseController {

	@Autowired
	ISysFunctionService functionService;
	
	/**
	 * 展示所有数据
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
		return new ModelAndView("auth/function/list.html", "list", functionService.findPage(pageNum, pageSize)); 
	}

	/**
	 * 跳转新增页面
	 * @return
	 */
	@GetMapping
	public ModelAndView saveUI() {
		ModelAndView mav = new ModelAndView("auth/function/save.html");
		mav.addObject("list", functionService.findFuncAll());
		return mav;
	}

	/**
	 * 点击新增
	 * @param func
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@PostMapping
	public ModelAndView save(@Valid SysFunction func,BindingResult result,RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView("redirect:/auth/function/list");
		if (result.hasErrors()) {
			mav = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mav;
		}
		func.setId(UUID.randomUUID().toString().replace("-", ""));
		functionService.save(func);
		return mav;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@DeleteMapping("/{id}")
	@ResponseBody	
	public AjaxVo delete(@PathVariable("id") String id){
		functionService.deleteById(id);
		return new AjaxVo().setErrorMsg("0");
	}

	/**
	 * 跳转编辑页面
	 * @param id
	 * @return
	 */
	@GetMapping("/updata/{id}")
	public ModelAndView updateUI(@PathVariable("id") String id) {
		ModelAndView mav = new ModelAndView("auth/function/update.html","func",functionService.findById(id));
		mav.addObject("list", functionService.findFuncAll());
		return mav;
	}

	/**
	 * 点击保存编辑
	 * @param func
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@PostMapping("/updata/{id}")
	public ModelAndView update(@Valid SysFunction func,BindingResult result,RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView("redirect:/auth/function/list");	
		if (result.hasErrors()) {
			ModelAndView mavs = new ModelAndView("redirect:/common/err");
			mavs = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mavs;
		}
		functionService.update(func);
		return mav;
	}
	
	/**
	 * 查看详情
	 * @param id
	 * @return
	 * @throws IOException 
	 */
	@GetMapping("/{id}")
	public ModelAndView detailUi(@PathVariable("id") String id) throws IOException {
		ModelAndView mav = new ModelAndView("auth/function/detail.html", "func", functionService.findById(id));
		mav.addObject("list", functionService.findFuncAll());
		return mav;
	}
}
