package com.zhidisoft.crm.controller.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.zhidisoft.crm.service.ISysLogService;

@Controller
@RequestMapping("auth/log")
public class SysLogController {

	@Autowired
	ISysLogService logService;
	/**
	 * 展示所有数据
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
		return new ModelAndView("auth/log/list.html", "list", logService.findPage(pageNum, pageSize)); 
	}
}
