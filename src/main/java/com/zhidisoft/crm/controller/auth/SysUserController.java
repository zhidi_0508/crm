package com.zhidisoft.crm.controller.auth;

import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.entity.SysUser;
import com.zhidisoft.crm.service.ISysUserService;
import com.zhidisoft.crm.vo.AjaxVo;

/**
 * auth/user/list get 默认列表(包括查询条件) <br/>
 * auth/user/${id} delete 删除<br/>
 * auth/user get 跳转到新增页面 <br/>
 * auth/user post data 新增<br/>
 * auth/user/${id} get 跳转到修改页面<br/>
 * auth/user/${id} post data 修改
 * @author MySpace
 *
 */
@Controller
@RequestMapping("auth/user")
public class SysUserController extends BaseController {

	@Autowired
	ISysUserService userService;
	
	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) {
		return new ModelAndView("auth/user/list.html", "list", userService.findPage(pageNum, pageSize)); 
	}
	
	/**
	 * 跳转到添加页面
	 * @return
	 */
	@GetMapping
	public String saveUi(){
		return "auth/user/save.html";
	}
	
	/**
	 * 添加页面保存按钮
	 * @param sysUser
	 * @return
	 */
	@PostMapping
	public ModelAndView save(@Valid SysUser sysUser,BindingResult result,RedirectAttributes redirectAttributes){
		ModelAndView mav = new ModelAndView("redirect:/auth/user/list");
		if (result.hasErrors()) {
			mav = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mav;
		}
		sysUser.setId(UUID.randomUUID().toString().replace("-", ""));
		sysUser.setUsername(sysUser.getUsername());
		StandardPasswordEncoder spe = new StandardPasswordEncoder();
		sysUser.setPassword(spe.encode(sysUser.getPassword()));
		userService.save(sysUser);
		return mav;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@DeleteMapping("/{id}")
	@ResponseBody	//页面如果用ajax请求的话，这里必须与页面一致，而且这里调用的delete注解，页面的type也要设置成delete;
	public AjaxVo delete(@PathVariable("id") String id){
		userService.deleteById(id);
		return new AjaxVo().setErrorMsg("0");
	}
	
	/**
	 * 跳转到编辑页面
	 * @param id
	 * @return
	 */
	@GetMapping("/{id}")
	public ModelAndView updateUi(@PathVariable("id") String id){
		SysUser user = userService.findById(id);
		return new ModelAndView("auth/user/update.html", "user", user);
	}
	
	/**
	 * 修改页面保存按钮
	 * @param sysUser
	 * @return
	 */
	@PostMapping("/{id}")
	public ModelAndView update(@Valid SysUser sysUser,BindingResult result,RedirectAttributes redirectAttributes){
		ModelAndView mav = new ModelAndView("redirect:/auth/user/list");
		if (result.hasErrors()) {
			mav = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mav;
		}
		userService.update(sysUser);
		return mav;
	}
	
}
