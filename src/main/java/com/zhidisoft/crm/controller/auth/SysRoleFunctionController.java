package com.zhidisoft.crm.controller.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.service.ISysFunctionService;
import com.zhidisoft.crm.service.ISysRoleFuncService;
import com.zhidisoft.crm.service.ISysRoleService;

@Controller
@RequestMapping("auth/role_func")
public class SysRoleFunctionController extends BaseController{
	
	@Autowired
	ISysRoleService roleService;
	
	@Autowired
	ISysFunctionService funcService;
	
	@Autowired
	ISysRoleFuncService roleFuncService;
	
	/**
	 * 跳转到分配菜单页面
	 * @param roleId
	 * @return
	 */
	@GetMapping("/{roleId}")
	public ModelAndView userRoleFrom(@PathVariable("roleId") String roleId){
		ModelAndView mav= new ModelAndView("auth/role_func/form.html", "role", roleService.findById(roleId));
		mav.addObject("functionList", funcService.findFuncAll());
		return mav;
	}
	
	/**
	 * 分配菜单页面保存按钮
	 * @param roleId
	 * @param funcIds
	 * @return
	 */
	@PostMapping("/{roleId}")
	public String save(@PathVariable("roleId") String roleId,String[] funcIds) {
		roleFuncService.save(roleId,funcIds);
		return "redirect:/auth/role/list";
	}

}
