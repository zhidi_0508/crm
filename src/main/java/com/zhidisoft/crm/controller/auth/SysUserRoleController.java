package com.zhidisoft.crm.controller.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.entity.SysUserRole;
import com.zhidisoft.crm.service.ISysRoleService;
import com.zhidisoft.crm.service.ISysUserRoleService;
import com.zhidisoft.crm.service.ISysUserService;

import com.zhidisoft.crm.util.UuidUtil;

@Controller
@RequestMapping("auth/user_role")
public class SysUserRoleController extends BaseController{
	@Autowired
	ISysUserRoleService userRoleService;
	
	@Autowired
	ISysRoleService roleService;
	
	@Autowired
	ISysUserService userService;
	
	/**
	 * 跳转到用户分配角色页面
	 * @param id
	 * @return
	 */
	@GetMapping("/{userId}")
	public ModelAndView userRoleFrom(@PathVariable("userId") String userId){
		ModelAndView mav= new ModelAndView("auth/user_role/form.html", "user", userService.findById(userId));
		mav.addObject("userRole", userRoleService.findById(userId));
		mav.addObject("roleList", roleService.showSysrole());
		return mav;
	}
	
	/**
	 * 给用户分配角色保存按钮
	 * @param id
	 * @param roleId
	 * @return
	 */
	@PostMapping("/{userId}")
	public String save(@PathVariable("userId") String userId,String roleId) {
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRole.setId(UuidUtil.uuid());
		sysUserRole.setUser_id(userId);
		sysUserRole.setRole_id(roleId);
		userRoleService.saveUserRole(sysUserRole);
		return "redirect:/auth/user/list";
	}
}
