package com.zhidisoft.crm.controller.business.recycleBin;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.service.IBusinessRecycleBinService;
import com.zhidisoft.crm.service.IBussinessService;
import com.zhidisoft.crm.service.ISysUserService;
import com.zhidisoft.crm.util.DateUtil;
import com.zhidisoft.crm.vo.BusinessOpportunityVo;
import com.zhidisoft.crm.vo.PageVo;

/**
 * business/list get 默认列表(包括查询条件) <br/>
 * business/${id} delete 删除<br/>
 * business get 跳转到新增页面 <br/>
 * business post data 新增<br/>
 * business/${id} get 跳转到修改页面<br/>
 * business/${id} post data 修改
 * 
 * @author MySpace
 *
 */
@Controller
@RequestMapping("/business/recycleBin")
public class RecycleBinController extends BaseController{

	@Autowired
	IBusinessRecycleBinService binService;

	@Autowired
	IBussinessService busService;
	
	@Autowired
	ISysUserService userService;
	
	@Autowired
	JavaMailSender mailSender;
	
	/**
	 * 商机分页列表
	 * 
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "CriteriaType", defaultValue = "-1") String CriteriaType,
			@RequestParam(name = "content", defaultValue = "") String content) throws IOException {
		content = new String(content.getBytes("ISO8859-1"), "UTF-8");
		if(CriteriaType=="-1"){
			content = "";
		}
		PageVo pageVo = binService.findPage(pageNum, pageSize,CriteriaType,content);
		ModelAndView mav = new ModelAndView("business/recycle_bin/list.html", "list", pageVo);
		mav.addObject("CriteriaType", CriteriaType);
		mav.addObject("content", content);
		return mav;
	}

	/**
	 * 批量恢复商机
	 * @param ids
	 */
	@DeleteMapping("/recover")
	@ResponseBody	
	public void deleteAll(@RequestBody String ids){
		String[] idArr = ids.split(",");//字符串转换为数组
		for (String string : idArr) {
			binService.recoverAll(string);
		}
	}

	/**
	 * 查看详情
	 * @param id
	 * @return
	 * @throws IOException 
	 */
	@GetMapping("/{id}")
	public ModelAndView detailUi(@PathVariable("id") String id) throws IOException {
		BusinessOpportunityVo vo = binService.findById(id);
		ModelAndView mav = new ModelAndView("business/recycle_bin/detail.html", "business", vo);
		mav.addObject("next_contact_time",DateUtil.parseStr(vo.getNext_contact_time(), "yyyy年MM月dd日"));
		mav.addObject("remark", new String(vo.getRemark(),"utf-8"));
		return mav;
	}

	
	/**
	 * 导入数据
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	@GetMapping("/import")
	public String importData() throws FileNotFoundException, IOException {
		StringBuffer fileName = new StringBuffer(session.getServletContext().getRealPath("/")).append("upload")
				.append(File.separatorChar).append("tmp").append(File.separatorChar).append("商机管理-")
				.append(DateUtil.parseStr(new Date(), "yyyy年MM月dd日")).append(".xls");
		HSSFSheet sheet = new HSSFWorkbook(new FileInputStream(fileName.toString())).getSheet("商机列表");

		// 高级写法
		for (int i = 0, length = sheet.getPhysicalNumberOfRows(); i < length; i++) {
			HSSFRow row = sheet.getRow(i);
			HSSFCell cell = row.getCell((short) 2);
			System.out.println(cell.getStringCellValue());
		}
		return "redirect:/business/recycle_bin/list";
	}
	
	/**
	 * 导出数据
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	@GetMapping("/export")
	public ResponseEntity<byte[]> exportData() throws IOException {
		// 生成文件名
		StringBuffer fileName = new StringBuffer(session.getServletContext().getRealPath("/")).append("upload")
				.append(File.separatorChar).append("tmp").append(File.separatorChar).append("商机管理-")
				.append(DateUtil.parseStr(new Date(), "yyyy年MM月dd日")).append(".xls");
		// 创建对Excel工作簿文件的引用
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 在Excel工作簿中建一工作表，其名为缺省值
		// 如要新建一名为"效益指标"的工作表，其语句为：
		// HSSFSheet sheet = workbook.createSheet("商机列表");
		HSSFSheet sheet = workbook.createSheet("商机列表");
		// 查询数据
		List<BusinessOpportunityVo> businesses = binService.findBusinessAll();
		
		// 0行是菜单
		HSSFRow row1 = sheet.createRow((short) 0);
		HSSFCell cell1 = row1.createCell((short) 1);
		cell1.setCellValue("序号");
		cell1 = row1.createCell((short) 2);
		cell1.setCellValue("客户");
		HSSFCell cell2 = row1.createCell((short) 3);
		cell2.setCellValue("商机名");
		HSSFCell cell4 = row1.createCell((short) 4);
		cell4.setCellValue("商机来源");
		HSSFCell cell5 = row1.createCell((short) 5);
		cell5.setCellValue("下次联系时间");
		HSSFCell cell6 = row1.createCell((short) 6);
		cell6.setCellValue("下次联系内容");
		HSSFCell cell7 = row1.createCell((short) 7);
		cell7.setCellValue("负责人");
		HSSFCell cell8 = row1.createCell((short) 8);
		cell8.setCellValue("创建人");
		HSSFCell cell9 = row1.createCell((short) 9);
		cell9.setCellValue("创建时间");
		HSSFCell cell10 = row1.createCell((short) 10);
		cell10.setCellValue("更新时间");
		
		int i = 1;
		for (BusinessOpportunityVo item : businesses) {
			HSSFRow row = sheet.createRow((short) i);
			cell1 = row.createCell((short) 1);
			cell1.setCellValue(i);
		    cell1 = row.createCell((short) 2);
			cell1.setCellValue(item.getCustomer_name());
			cell2 = row.createCell((short) 3);
			cell2.setCellValue(item.getBusiness_name());
			cell4 = row.createCell((short) 4);
			cell4.setCellValue(item.getSource_name());
			cell5 = row.createCell((short) 5);
			cell5.setCellValue(DateUtil.parseStr(item.getNext_contact_time(), "yyyy年MM月dd日"));
			cell6 = row.createCell((short) 6);
			cell6.setCellValue(item.getNext_contact_info());
			cell7 = row.createCell((short) 7);
			cell7.setCellValue(item.getCharge_user_name());
			cell8 = row.createCell((short) 8);
			cell8.setCellValue(item.getCreate_user_name());
			cell9 = row.createCell((short) 9);
			cell9.setCellValue(DateUtil.parseStr(item.getCreate_time(), "yyyy年MM月dd日"));
			cell10 = row.createCell((short) 10);
			if (item.getUpdate_time()!=null) {
				cell10.setCellValue(DateUtil.parseStr(item.getUpdate_time(), "yyyy年MM月dd日"));
			}
			i++;
		}
		// 新建一输出文件流
		File file = new File(fileName.toString());
		if (!file.exists())
			file.createNewFile();
		workbook.write(file);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDispositionFormData("attachment", file.getName(), Charset.forName("UTF-8"));// 防止文件名乱码
		HttpStatus statusCode = HttpStatus.OK;
		ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>
			(FileUtils.readFileToByteArray(file), headers, statusCode);
		return entity;
	}
}
