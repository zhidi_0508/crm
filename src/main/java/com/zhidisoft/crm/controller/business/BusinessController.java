package com.zhidisoft.crm.controller.business;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.controller.template.DistrictController;
import com.zhidisoft.crm.entity.BusinessOpportunity;
import com.zhidisoft.crm.service.IBusinessBoostService;
import com.zhidisoft.crm.service.IBusinessOpportunityService;
import com.zhidisoft.crm.service.IBussinessService;
import com.zhidisoft.crm.service.ISysUserService;
import com.zhidisoft.crm.util.DateUtil;
import com.zhidisoft.crm.vo.BusinessOpportunityFormVo;
import com.zhidisoft.crm.vo.BusinessOpportunityVo;
import com.zhidisoft.crm.vo.PageVo;

/**
 * business/list get 默认列表(包括查询条件) <br/>
 * business/${id} delete 删除<br/>
 * business get 跳转到新增页面 <br/>
 * business post data 新增<br/>
 * business/${id} get 跳转到修改页面<br/>
 * business/${id} post data 修改
 * 
 * @author MySpace
 *
 */
@Controller
@RequestMapping("/business")
public class BusinessController extends BaseController{

	@Autowired
	IBusinessOpportunityService businessService;

	@Autowired
	IBussinessService busService;
	
	@Autowired
	ISysUserService userService;
	
	@Autowired
	IBusinessBoostService boostService;
	
	@Autowired
	JavaMailSender mailSender;
	
	/**
	 * 获取商机报表统计
	 * @return
	 */
	@GetMapping("/echarts")
	public ModelAndView echarts() {
		ModelAndView mav = new ModelAndView("business/echarts.html");
		String firstDayOfWeek = DateUtil.getFirstDayOfWeek().toString();
		String lastDayOfWeek = DateUtil.getLastDayOfWeek().toString();
		String firstDayOfMonth = DateUtil.getFirstDayOfMonth().toString();
		String lastDayOfMonth = DateUtil.getLastDayOfMonth().toString();
		
		//本周商机统计
		mav.addObject("countData", businessService.countDay(firstDayOfWeek, lastDayOfWeek));
		//本月商机统计
		mav.addObject("countByMonth", businessService.countDay(firstDayOfMonth, lastDayOfMonth));
		
		return mav;
	}
	
	/**
	 * 商机分页列表
	 * @param pageNum
	 * @param pageSize
	 * @return
	 * @throws IOException 
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "CriteriaType", defaultValue = "-1") String CriteriaType,
			@RequestParam(name = "content", defaultValue = "") String content) throws IOException {
		content = new String(content.getBytes("ISO8859-1"), "UTF-8");
		if(CriteriaType=="-1"){
			content = "";
		}
		PageVo pageVo = businessService.findPage(pageNum, pageSize,CriteriaType,content);
		ModelAndView mav = new ModelAndView("business/list.html", "list", pageVo);
		mav.addObject("CriteriaType", CriteriaType);
		mav.addObject("content", content);
		return mav;
	}

	/**
	 * 批量删除商机
	 * @param ids
	 */
	@DeleteMapping("/delete")
	@ResponseBody	
	public void deleteAll(@RequestBody String ids){
		String[] idArr = ids.split(",");//字符串转换为数组
		for (String string : idArr) {
			businessService.deleteAll(string);
		}
	}

	/**
	 * 跳转到添加商机页面
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	@GetMapping("/save")
	public ModelAndView saveUi() throws IOException{
		ModelAndView mav = new ModelAndView("business/save.html","provinceList",new DistrictController().list());
		mav.addObject("types", busService.findTypeAll());
		mav.addObject("status", busService.findStatusAll());
		mav.addObject("sources", busService.findSourceAll());
		mav.addObject("customers", busService.findCustomerAll());
		mav.addObject("users", userService.findUserAll());
		return mav;
	}

	/**
	 * 
	 * 添加页面保存按钮
	 */
	@PostMapping("/save")
	public ModelAndView addbusiness(@Valid BusinessOpportunityFormVo busiformVo
			,BindingResult result,RedirectAttributes redirectAttributes){
		ModelAndView mav = new ModelAndView("redirect:/business/list");
		if (result.hasErrors()) {
			mav = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mav;
		}
		BusinessOpportunity busi = new BusinessOpportunity();
		busi.setCharge_user_id(userService.getUser(busiformVo.getCharge_username()).getId());
		busi.setCustomer_id(busiformVo.getCustomer_id());
		busi.setBusiness_money(busiformVo.getBusiness_money());
		busi.setBusiness_name(busiformVo.getBusiness_name());
		busi.setContact_name(busiformVo.getContact_name());
		busi.setContract_address(busiformVo.getProvince()+","+busiformVo.getCity()+","+busiformVo.getArea()+","+busiformVo.getAreac());
		busi.setBusiness_type_id(busiformVo.getBusiness_type_id());//商机类型
		busi.setBusiness_status_id(busiformVo.getBusiness_status_id());//状态
		busi.setBusiness_source_id(busiformVo.getBusiness_source_id());
		busi.setWin_rate(busiformVo.getWin_rate());
		busi.setExpected_transaction_price(busiformVo.getExpected_transaction_price());
		busi.setNext_contact_time(DateUtil.toDate(busiformVo.getNext_contact_time()));
		busi.setNext_contact_info(busiformVo.getNext_contact_info());
		busi.setRemark(busiformVo.getRemark().getBytes());
		busi.setCreate_user_id(session.getAttribute("userId").toString());
		businessService.save(busi);
		return mav;
	}

	/**
	 * 查看详情
	 * @param id
	 * @return
	 * @throws IOException 
	 */
	@GetMapping("/{id}")
	public ModelAndView detailUi(@PathVariable("id") String id) throws IOException {
		BusinessOpportunityVo vo = businessService.findById(id);
		ModelAndView mav = new ModelAndView("business/detail.html", "business", vo);
		mav.addObject("next_contact_time",DateUtil.parseStr(vo.getNext_contact_time(), "yyyy年MM月dd日"));
		mav.addObject("remark", new String(vo.getRemark(),"utf-8"));
		return mav;
	}

	/**
	 * 跳转到编辑页面
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/update/{id}")
	public ModelAndView updateUi(@PathVariable("id") String id) throws IOException {
		BusinessOpportunity business = businessService.findByBusiness(id);
		ModelAndView mav = new ModelAndView("business/update.html", "business", business);
		List<String> provinceList = new ArrayList<>();
		// 获取httpClient
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		// 创建一个GET请求
		HttpGet httpget = new HttpGet(
				"http://restapi.amap.com/v3/config/district?key=9c161363abdaf80db6c56fa8f4472bf4");
		// 执行GET请求
		HttpResponse response = httpclient.execute(httpget);
		// if(response.getEntity().getContent())
		// 把inputstream 转换为字符串 ---- json对象
		String content = IOUtils.toString(response.getEntity().getContent(), "utf-8");
		// 把字符串转换为json对象
		JSONObject object = JSON.parseObject(content);
		// 解析下一级数据
		JSONArray array1 = object.getJSONArray("districts");
		JSONArray array2 = array1.getJSONObject(0).getJSONArray("districts");

		// 循环
		Iterator<Object> iterator = array2.iterator();
		while (iterator.hasNext()) {
			JSONObject item = (JSONObject) iterator.next();
			provinceList.add(item.getString("name"));
		}
		
		String[] address = business.getContract_address().split(",");
		String state = address[0];//省份
		String city = address[1];//城市
		String area = address[2];//地区
		
		String street ="";
		if(address.length==4){
			street = address[3];//街道
		}
		mav.addObject("types", busService.findTypeAll());
		mav.addObject("status", busService.findStatusAll());
		mav.addObject("sources", busService.findSourceAll());
		mav.addObject("customers", busService.findCustomerAll());
		mav.addObject("user", userService.findById(business.getCharge_user_id()));
		mav.addObject("provinceList", new DistrictController().list());
		mav.addObject("state", state);
		mav.addObject("city", city);
		mav.addObject("area", area);
		mav.addObject("street", street);
		mav.addObject("remark", new String(business.getRemark(),"utf-8"));

		return mav;
	}

	/**
	 * 编辑页面保存按钮
	 * @param business
	 * @return
	 */
	@PostMapping("/update/{id}")
	public ModelAndView updateBusiness(String id, @Valid BusinessOpportunityFormVo vo
			,BindingResult result,RedirectAttributes redirectAttributes) {
		ModelAndView mav = new ModelAndView("redirect:/business/list");
		if (result.hasErrors()) {
			mav = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mav;
		}
		BusinessOpportunity busi = new BusinessOpportunity();
		busi.setId(id);
		busi.setCharge_user_id(userService.getUser(vo.getCharge_username()).getId());
		busi.setCustomer_id(vo.getCustomer_id());
		busi.setBusiness_money(vo.getBusiness_money());
		busi.setBusiness_name(vo.getBusiness_name());
		busi.setContact_name(vo.getContact_name());
		busi.setContract_address(vo.getProvince()+","+vo.getCity()+","+vo.getArea()+","+vo.getAreac());
		busi.setBusiness_type_id(vo.getBusiness_type_id());//商机类型
		busi.setBusiness_status_id(vo.getBusiness_status_id());//状态
		busi.setBusiness_source_id(vo.getBusiness_source_id());
		busi.setWin_rate(vo.getWin_rate());
		busi.setExpected_transaction_price(vo.getExpected_transaction_price());
		busi.setNext_contact_time(DateUtil.toDate(vo.getNext_contact_time()));
		busi.setNext_contact_info(vo.getNext_contact_info());
		busi.setRemark(vo.getRemark().getBytes());
		busi.setCreate_user_id(session.getAttribute("userId").toString());
		busi.setUpdate_time(new Date());
		businessService.updateById(busi);
		return mav;
	}
	
	/**
	 * 导入数据
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	@GetMapping("/import")
	public String importData() throws FileNotFoundException, IOException {
		StringBuffer fileName = new StringBuffer(session.getServletContext().getRealPath("/")).append("upload")
				.append(File.separatorChar).append("tmp").append(File.separatorChar).append("商机管理-")
				.append(DateUtil.parseStr(new Date(), "yyyy年MM月dd日")).append(".xls");
		HSSFSheet sheet = new HSSFWorkbook(new FileInputStream(fileName.toString())).getSheet("商机列表");

		// 高级写法
		for (int i = 0, length = sheet.getPhysicalNumberOfRows(); i < length; i++) {
			HSSFRow row = sheet.getRow(i);
			HSSFCell cell = row.getCell((short) 2);
			System.out.println(cell.getStringCellValue());
		}
		return "redirect:/business/list";
	}
	
	/**
	 * 导出数据
	 * @return
	 * @throws IOException
	 */
	@SuppressWarnings({ "resource", "unchecked" })
	@GetMapping("/export")
	public ResponseEntity<byte[]> exportData() throws IOException {
		// 生成文件名
		StringBuffer fileName = new StringBuffer(session.getServletContext().getRealPath("/")).append("upload")
				.append(File.separatorChar).append("tmp").append(File.separatorChar).append("商机管理-")
				.append(DateUtil.parseStr(new Date(), "yyyy年MM月dd日")).append(".xls");
		// 创建对Excel工作簿文件的引用
		HSSFWorkbook workbook = new HSSFWorkbook();
		// 在Excel工作簿中建一工作表，其名为缺省值
		// 如要新建一名为"商机列表"的工作表，其语句为：
		HSSFSheet sheet = workbook.createSheet("商机列表");
		sheet.autoSizeColumn(1, true);//自适应列宽度 
		// 查询数据
		List<BusinessOpportunityVo> businesses = businessService.findPage(1, businessService.BusinessCount(), "-1", "").getList();
		
		// 0行是菜单
		HSSFRow row1 = sheet.createRow((short) 0);
		HSSFCell cell1 = row1.createCell((short) 1);
		cell1.setCellValue("序号");
		cell1 = row1.createCell((short) 2);
		cell1.setCellValue("客户");
		HSSFCell cell2 = row1.createCell((short) 3);
		cell2.setCellValue("商机名");
		HSSFCell cell4 = row1.createCell((short) 4);
		cell4.setCellValue("商机来源");
		HSSFCell cell5 = row1.createCell((short) 5);
		cell5.setCellValue("下次联系时间");
		HSSFCell cell6 = row1.createCell((short) 6);
		cell6.setCellValue("下次联系内容");
		HSSFCell cell7 = row1.createCell((short) 7);
		cell7.setCellValue("负责人");
		HSSFCell cell8 = row1.createCell((short) 8);
		cell8.setCellValue("创建人");
		HSSFCell cell9 = row1.createCell((short) 9);
		cell9.setCellValue("创建时间");
		HSSFCell cell10 = row1.createCell((short) 10);
		cell10.setCellValue("更新时间");
		
		int i = 1;
		for (BusinessOpportunityVo item : businesses) {
			HSSFRow row = sheet.createRow((short) i);
			cell1 = row.createCell((short) 1);
			cell1.setCellValue(i);
		    cell1 = row.createCell((short) 2);
			cell1.setCellValue(item.getCustomer_name());
			cell2 = row.createCell((short) 3);
			cell2.setCellValue(item.getBusiness_name());
			cell4 = row.createCell((short) 4);
			cell4.setCellValue(item.getSource_name());
			cell5 = row.createCell((short) 5);
			cell5.setCellValue(DateUtil.parseStr(item.getNext_contact_time(), "yyyy年MM月dd日"));
			cell6 = row.createCell((short) 6);
			cell6.setCellValue(item.getNext_contact_info());
			cell7 = row.createCell((short) 7);
			cell7.setCellValue(item.getCharge_user_name());
			cell8 = row.createCell((short) 8);
			cell8.setCellValue(item.getCreate_user_name());
			cell9 = row.createCell((short) 9);
			cell9.setCellValue(DateUtil.parseStr(item.getCreate_time(), "yyyy年MM月dd日"));
			cell10 = row.createCell((short) 10);
			if (item.getUpdate_time()!=null) {
				cell10.setCellValue(DateUtil.parseStr(item.getUpdate_time(), "yyyy年MM月dd日"));
			}
			i++;
		}
		// 新建一输出文件流
		File file = new File(fileName.toString());
		if (!file.exists())
			file.createNewFile();
		workbook.write(file);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		headers.setContentDispositionFormData("attachment", file.getName(), Charset.forName("UTF-8"));// 防止文件名乱码
		HttpStatus statusCode = HttpStatus.OK;
		ResponseEntity<byte[]> entity = new ResponseEntity<byte[]>
			(FileUtils.readFileToByteArray(file), headers, statusCode);
		return entity;
	}
	
	/**
	 * 发送邮件
	 * @return
	 * @throws MessagingException
	 */
	@GetMapping("/email")
	public String email() throws MessagingException{
		return "redirect:/register";
	}
}
