package com.zhidisoft.crm.controller.business;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.entity.BusinessOpportunity;
import com.zhidisoft.crm.service.IBusinessBoostService;
import com.zhidisoft.crm.service.IBusinessOpportunityService;
import com.zhidisoft.crm.service.IBussinessService;
import com.zhidisoft.crm.util.DateUtil;
import com.zhidisoft.crm.vo.BusinessBoostListVo;
import com.zhidisoft.crm.vo.BusinessBoostVo;
import com.zhidisoft.crm.vo.BusinessOpportunityVo;
import com.zhidisoft.crm.vo.PageVo;

@Controller
@RequestMapping("/business/boost")
public class BusinessBoostLayerController extends BaseController {

	@Autowired
	IBussinessService service;
	
	@Autowired
	IBusinessOpportunityService tyService;
	
	@Autowired
	IBusinessBoostService boostService;
	
	/**
	 * 跳转到推进页面(弹窗)
	 * @param id
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/update/{id}")
	public ModelAndView updateUi(@PathVariable("id") String id) throws IOException{
		BusinessOpportunityVo vo = tyService.findById(id);
		ModelAndView mav = new ModelAndView("business/boost/boost_update.html","statusAll",service.findStatusAll());
		mav.addObject("status", service.findStatus(id));
		mav.addObject("business", vo);
		mav.addObject("remark", new String(vo.getRemark(),"utf-8"));
		return mav;
		
		
	}
	
	/**
	 * 商机推进到产品
	 * @param boostVo
	 * @param result
	 * @param redirectAttributes
	 * @return
	 */
	@PostMapping("/update/{id}")
	public ModelAndView update(@Valid BusinessBoostVo boostVo, BindingResult result,
			RedirectAttributes redirectAttributes){
		ModelAndView mav = new ModelAndView("redirect:/business/list");
		if (result.hasErrors()) {
			mav = new ModelAndView("redirect:/common/err");
			redirectAttributes.addFlashAttribute("errorList", result.getAllErrors());
			return mav;
			
		}
		BusinessOpportunity business = new BusinessOpportunity();
		business.setId(boostVo.getID());
		business.setBusiness_status_id(boostVo.getBUSINESS_STATUS_ID());
		business.setNext_contact_time(DateUtil.toDate(boostVo.getNEXT_CONTACT_TIME()));
		business.setNext_contact_info(boostVo.getNEXT_CONTACT_INFO());
		business.setRemark(boostVo.getDESCRIPTION().getBytes());
		business.setCharge_user_id(session.getAttribute("userId").toString());
		boolean flag = tyService.updateBoostById(business);
		mav.addObject("flag", flag);
		return mav;
	}
	
	/**
	 * 推进列表(历史)
	 * @param pageNum
	 * @param pageSize
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/list")
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) throws IOException {
		PageVo pageVo = boostService.findPage(pageNum, pageSize);
		ModelAndView mav = new ModelAndView("business/boost/list.html", "list", pageVo);
		return mav;
	}
	
	/**
	 * 推进历史详情
	 * @param id
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/detail/{id}")
	public ModelAndView detailUi(@PathVariable("id") String id) throws IOException {
		BusinessBoostListVo boost = boostService.findById(id);
		ModelAndView mav = new ModelAndView("business/boost/detail.html", "boost", boost);
		mav.addObject("NEXT_CONTACT_INFO", new String(boost.getNext_contact_info(), "utf-8"));
		mav.addObject("DESCRIPTION", new String(boost.getDescription(), "utf-8"));
		return mav;
	}
}
