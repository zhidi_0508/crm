package com.zhidisoft.crm.controller.template;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.zhidisoft.crm.service.ISysUserService;
/**
 * 用户弹出窗
 * @author MySpace
 *
 */
@Controller
@RequestMapping("/charge/user/layer")
public class ChargeUserLayerController {

	@Autowired
	ISysUserService sysUserService;

	@GetMapping
	public ModelAndView list(@RequestParam(name = "pageNum", defaultValue = "1") Integer pageNum,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "username", defaultValue = "") String username) throws UnsupportedEncodingException {
		// Get 中文请求处理
		username = new String(username.getBytes("ISO8859-1"), "UTF-8");
		ModelAndView mav = new ModelAndView("charge/user/layer.html", "list",
				sysUserService.findPage(pageNum, pageSize, username));
		return mav; // 省代码(视图解析器)
	}
}
