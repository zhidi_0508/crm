package com.zhidisoft.crm.controller.template;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

//git.oschina.net/ZiSeXuanFeng/crm.git
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController {

	@Autowired
	public HttpSession session;

	public void setSessionAttr(String name, Object value) {
		session.setAttribute(name, value);
	}

	@Autowired
	public HttpServletRequest request;

	public void setRequestAttr(String name, Object value) {
		request.setAttribute(name, value);
	}

	public String getCurUser() {
		Object userObj = session.getAttribute("userId");
		if (userObj == null)
			return "";
		return userObj.toString();
	}

}
