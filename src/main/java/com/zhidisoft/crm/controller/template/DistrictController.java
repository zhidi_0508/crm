package com.zhidisoft.crm.controller.template;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
/**
 * 行政区api
 * @author MySpace
 *
 */
@Controller
@RequestMapping("/district")
public class DistrictController {

	public List<String> list() throws ClientProtocolException, IOException {
		List<String> provinceList = new ArrayList<>();

		// 获取httpClient
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		// 创建一个GET请求
		HttpGet httpget = new HttpGet(
				"http://restapi.amap.com/v3/config/district?key=9c161363abdaf80db6c56fa8f4472bf4");
		// 执行GET请求
		HttpResponse response = httpclient.execute(httpget);
		// if(response.getEntity().getContent())
		// 把inputstream 转换为字符串 ---- json对象
		String content = IOUtils.toString(response.getEntity().getContent(), "utf-8");
		// 把字符串转换为json对象
		JSONObject object = JSON.parseObject(content);
		// 解析下一级数据
		JSONArray array1 = object.getJSONArray("districts");
		JSONArray array2 = array1.getJSONObject(0).getJSONArray("districts");

		// 循环
		Iterator<Object> iterator = array2.iterator();
		while (iterator.hasNext()) {
			JSONObject item = (JSONObject) iterator.next();
			provinceList.add(item.getString("name"));
		}
		return provinceList;
	}

	/**
	 * ajax get 中文参数 乱码
	 * 
	 * @param name
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@PostMapping("/data_ajax")
	@ResponseBody
	public List<String> data_ajax(String name) throws ClientProtocolException, IOException {
		List<String> dataList = new ArrayList<>();
		// 获取httpClient
		CloseableHttpClient httpclient = HttpClientBuilder.create().build();
		// 创建一个GET请求
		HttpGet httpget = new HttpGet(
				"http://restapi.amap.com/v3/config/district?key=9c161363abdaf80db6c56fa8f4472bf4&keywords=" + name);
		// 执行GET请求
		HttpResponse response = httpclient.execute(httpget);
		// if(response.getEntity().getContent())
		// 把inputstream 转换为字符串 ---- json对象
		String content = IOUtils.toString(response.getEntity().getContent());
		// 把字符串转换为json对象
		JSONObject object = JSON.parseObject(content);
		// 解析下一级数据
		JSONArray array1 = object.getJSONArray("districts");
		JSONArray array2 = array1.getJSONObject(0).getJSONArray("districts");

		// 循环
		Iterator<Object> iterator = array2.iterator();
		while (iterator.hasNext()) {
			JSONObject item = (JSONObject) iterator.next();
			dataList.add(item.getString("name"));
		}
		return dataList;
	}
}
