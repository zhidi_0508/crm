package com.zhidisoft.crm.controller.template;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * 异常提示
 * @author MySpace
 *
 */
@Controller
@RequestMapping("/common")
public class CommonController {

	@GetMapping("/err")
	public String err(){
		return "/common/error.html";
	}
}
