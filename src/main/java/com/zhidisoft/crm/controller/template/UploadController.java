package com.zhidisoft.crm.controller.template;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.zhidisoft.crm.util.UuidUtil;
import com.zhidisoft.crm.vo.AjaxVo;

/**
 * 上传功能
 * 
 * @author TeacherRen
 *
 */
@Controller
@RequestMapping("/uploadImg")
public class UploadController extends BaseController {

	@GetMapping
	public String uploadUI() {
		return "upload.html";
	}

	/**
	 * MultipartFile file 页面传送过来的文件
	 * UuidUtil.uuid() 获取随机id
	 *  
	 * @param file
	 * @return
	 * @throws IOException
	 * @throws IllegalStateException
	 */
	@PostMapping
	@ResponseBody
	public AjaxVo upload(MultipartFile file) throws IllegalStateException, IOException {
		// 获取文件后缀
		String filePostfix = file.getOriginalFilename();
		// 生成文件名
		String fileName = UuidUtil.uuid() + "." + FilenameUtils.getExtension(filePostfix);

		// 获取文件路径
		String uploadPath = session.getServletContext().getRealPath("/upload");
		// 新的文件
		File newFile = new File(uploadPath + File.separatorChar + fileName);
		if (!newFile.exists())
			newFile.createNewFile();
		// 保存文件
		file.transferTo(newFile);
		return new AjaxVo().setErrorCode("0").setData(fileName);
	}
}
