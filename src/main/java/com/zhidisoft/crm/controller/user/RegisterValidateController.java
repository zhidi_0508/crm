package com.zhidisoft.crm.controller.user;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zhidisoft.crm.controller.template.BaseController;

/**
 * 验证激活
 * 
 * @author TeacherRen
 *
 */
@Controller
@RequestMapping("/register/validate")
public class RegisterValidateController extends BaseController {

	@GetMapping
	public String index(String email, String validateCode, RedirectAttributes redirectAttributes) {
		// TODO 验证代码
		redirectAttributes.addFlashAttribute("msg", "激活成功");
		return "redirect:/register";
	}
}
