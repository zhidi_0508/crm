package com.zhidisoft.crm.controller.user;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import javax.mail.MessagingException;

import org.beetl.core.GroupTemplate;
import org.beetl.core.Template;
import org.beetl.ext.spring.BeetlGroupUtilConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.zhidisoft.crm.controller.template.BaseController;
import com.zhidisoft.crm.vo.AjaxVo;

@Controller
@RequestMapping("/register")
public class RegisterController extends BaseController {

	@Autowired
	JmsTemplate jmsTemplate;

	@Autowired
	BeetlGroupUtilConfiguration beetlConfig;

	@GetMapping
	public ModelAndView index() {
		return new ModelAndView("/register.html");
	}

	@PostMapping
	@ResponseBody
	public AjaxVo register(String email) throws MessagingException {

		// 生成激活链接
		String validateCodeLink = "http://localhost:8080/crm/register/validate?email=" + email + "&validateCode="
				+ "313799008";

		GroupTemplate gt = beetlConfig.getGroupTemplate();
		Template t = gt.getTemplate("/common/email/_register.html");
		t.binding("validateCodeLink", validateCodeLink);
		String htmlResult = t.render();	

		// 调用发注册邮件代码
		jmsTemplate.send(new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				MapMessage msg = session.createMapMessage();	//将数据存入MapMessage中
				msg.setString("email", email);
				msg.setString("htmlResult", htmlResult);
				return msg;
			}
		});
		return new AjaxVo().setErrorCode("0");
	}
	
}
