package com.zhidisoft.crm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zhidisoft.crm.entity.SysUser;
import com.zhidisoft.crm.vo.SysUserVo;

/**
 * 用户登陆的mapper
 * @author MySpace
 *
 */
public interface ISysUserMapper {

	/**
	 * 新增用户
	 * @param user
	 */
	@Insert("insert into sys_user(id,username,password,is_admin) values(#{id},#{username},#{password},#{is_admin,jdbcType=VARCHAR})")
	int save(SysUser sysUser);

	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@Delete("delete from sys_user where id = #{id}")
	public int deleteById(String id);
	
	/**
	 * 根据用户名获取用户
	 * @param username
	 * @return
	 */
	@Select("select * from sys_user where username=#{username}")
	SysUser findByUserName(String username);
	
	/**
	 * 根据id获取用户
	 * @param id
	 * @return
	 */
	@Select("select * from sys_user where id=#{id}")
	SysUser findById(String id);
	
	/**
	 * 分页查询
	 * oracle查询
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select u.id,u.username,u.create_time,u.is_admin,r.role_name from sys_user u"
			+ " left join sys_user_role ur on ur.user_id=u.id left join sys_role r on ur.role_id=r.id ) A"
			+ " WHERE ROWNUM <= #{endIndex})  TL "
			+ "WHERE RN > #{startIndex}")
	List<SysUserVo> pageList(@Param("startIndex") Integer startIndex, @Param("endIndex") Integer endIndex);

	
	@Select("<script>"
			+" select * from   (       "
			+ "  	select A.* ,ROWNUM rn"
			+ "		from ("
			+ "				select *   from SYS_USER"
			+ "            	<where>"
			+ "               	 	<if  test='username != null and username!=\"\"' >"
			+"								AND username=#{username}"
			+ "					</if>"
			+ "            	</where> "
			+ "				) A"
			+ "		<if test='endIndex != null and endIndex!=\"\"' >"
			+ " 			<![CDATA[where ROWNUM <=#{endIndex}]]>"
			+ "		</if>  )"
			+ " <if test='beginIndex != null and beginIndex!=\"\"' >"
			+ " 		Where rn > #{beginIndex}"
			+ " </if>"
			+"</script>")
	List<SysUser> pageListWhere(@Param("beginIndex")Integer beginIndex, @Param("endIndex")Integer endIndex,@Param("username") String username);

	
	/**
	 * 获取总条数
	 * @return
	 */
	@Select("SELECT count(id) FROM sys_user")
	Long totalCount();

	/**
	 * 获取总条数
	 * @return
	 */
	@Select("<script>SELECT count(id) FROM sys_user" + " where 1=1"
			+ " <if test='username != \"\"'> and username=#{username}</if></script>")
	Long totalCountWhere(@Param("username") String username);
	
	/**
	 * 修改
	 * @param sysUser
	 * @return
	 */
	@Update("update sys_user set username=#{username},is_admin=#{is_admin} where id=#{id}")
	int update(SysUser sysUser);

	/**
	 * 查询全部用户
	 * @return
	 */
	@Select("select * from sys_user")
	List<SysUser> findUserAll();

	/**
	 * 根据负责人查询用户
	 * @param charge_username
	 * @return
	 */
	@Select("select * from sys_user where username=#{charge_username}")
	SysUser getUser(String charge_username);
	
}
