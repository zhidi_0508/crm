package com.zhidisoft.crm.mapper;

import java.math.BigInteger;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zhidisoft.crm.entity.BusinessOpportunity;
import com.zhidisoft.crm.vo.BusinessCountVo;
import com.zhidisoft.crm.vo.BusinessOpportunityVo;

public interface IBusinessOpportunityMapper {

	/**
	 * 添加商机
	 * @param business
	 * @return
	 */
	@Insert("insert into BUSINESS_OPPORTUNITY(CUSTOMER_ID,BUSINESS_MONEY,BUSINESS_NAME,CONTACT_NAME,CONTRACT_ADDRESS,BUSINESS_TYPE_ID,BUSINESS_STATUS_ID,BUSINESS_SOURCE_ID"
			+ ",WIN_RATE,EXPECTED_TRANSACTION_PRICE,NEXT_CONTACT_TIME,NEXT_CONTACT_INFO,REMARK,CHARGE_USER_ID,CREATE_USER_ID)"
			+ " values (#{customer_id,jdbcType=VARCHAR},#{business_money,jdbcType=NUMERIC},#{business_name,jdbcType=VARCHAR}"
			+ ",#{contact_name,jdbcType=VARCHAR},#{contract_address,jdbcType=VARCHAR},#{business_type_id,jdbcType=VARCHAR}"
			+ ",#{business_status_id,jdbcType=VARCHAR},#{business_source_id,jdbcType=VARCHAR}"
			+ ",#{win_rate,jdbcType=NUMERIC},#{expected_transaction_price,jdbcType=NUMERIC},#{next_contact_time,jdbcType=DATE}"
			+ ",#{next_contact_info,jdbcType=VARCHAR},#{remark},#{charge_user_id,jdbcType=VARCHAR},#{create_user_id,jdbcType=VARCHAR})")
	int add(BusinessOpportunity business);
	
	/**
	 * 删除商机
	 * @param ids
	 */
	@Delete("update business_opportunity set removeState=1 where id=#{id}")
	void deleteAll(String ids);

	/**
	 * 修改商机信息
	 * @return
	 */
	@Update("update BUSINESS_OPPORTUNITY set charge_user_id=#{charge_user_id}"
			+ ",create_user_id=#{create_user_id},customer_id=#{customer_id}"
			+ ",business_money=#{business_money},business_name=#{business_name}"
			+ ",contact_name=#{contact_name},contract_address=#{contract_address}"
			+ ",business_status_id=#{business_status_id},business_source_id=#{business_source_id}"
			+ ",win_rate=#{win_rate},expected_transaction_price=#{expected_transaction_price}"
			+ ",next_contact_time=#{next_contact_time},next_contact_info=#{next_contact_info}"
			+ ",remark=#{remark,jdbcType=BLOB},update_time=#{update_time} where id=#{id}")
	int update(BusinessOpportunity business);
	
	/**
	 * 商机推进产品(修改商机内容)
	 * @param business
	 * @return
	 */
	@Update("update BUSINESS_OPPORTUNITY set business_status_id=#{business_status_id,jdbcType=VARCHAR}"
			+ ",next_contact_time=#{next_contact_time,jdbcType=DATE},next_contact_info=#{next_contact_info,jdbcType=VARCHAR}"
			+ ",remark=#{remark,jdbcType=BLOB} where id=#{id,jdbcType=VARCHAR}")
	int updateBoostById(BusinessOpportunity business);
	
	/**
	 * 根据id查询一条商机
	 * @param id
	 * @return
	 */
	@Select("select * from BUSINESS_OPPORTUNITY where id = #{id}")
	BusinessOpportunity findByBusiness(String id);
	
	/**
	 * 根据id查询数据(详情)
	 * @param id
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select t.*,u_charge.username charge_user_name,u_create.username create_user_name"
			+ ",c.customer_name,s.source_name,o.type_name,bos.status_name from BUSINESS_OPPORTUNITY t "
			+ "left join sys_user u_charge on t.charge_user_id=u_charge.id "
			+ "left join sys_user u_create on t.create_user_id=u_create.id "
			+ "left join customer c on t.customer_id=c.id "
			+ "left join business_opportunity_status bos on t.business_status_id=bos.id "
			+ "left join business_opportunity_source s on t.business_source_id=s.id "
			+ "left join business_opportunity_type o on t.business_type_id=o.id ) A"
			+ ") WHERE ID = #{id}")
	BusinessOpportunityVo findById(String id);
	
	/**
	 * 根据商机名获取一条商机
	 * @param business_name
	 * @return
	 */
	@Select("select * from BUSINESS_OPPORTUNITY where business_name=#{business_name}")
	BusinessOpportunity findBusiness(String business_name);
	
	/**
	 * 获取所有商机
	 * @return
	 */
	@Select("select * from BUSINESS_OPPORTUNITY")
	List<BusinessOpportunityVo> findBusinessAll();
	
	/**
	 * 分页查询
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	@Select("<script>"
			+ "SELECT bo.*,u_charge.username charge_user_name,u_create.username create_user_name"
			+ ",c.customer_name,s.source_name FROM ("
			+ "	SELECT A.*,ROWNUM rn "
			+ "		FROM ("
			+ " 	select * FROM BUSINESS_OPPORTUNITY "
			+ " 	<choose>  "
            + "			<when test='CriteriaType==\"business_name\"'>  "
           	+ "    			where business_name like '%'||#{content}||'%'  "
            + " 		</when>  "
            + "		</choose>  "
			+ "		) A" 
			+ "	 <if test='endIndex != null and endIndex!=\"\"' >"
			+ " 	<![CDATA[where ROWNUM <=#{endIndex} and REMOVESTATE=0]]>"
			+ "	 </if>  ) bo"
			+ "		left join sys_user u_charge on bo.charge_user_id=u_charge.id "
			+ "		left join sys_user u_create on bo.create_user_id=u_create.id "
			+ "		left join customer c on bo.customer_id=c.id "
			+ "		left join business_opportunity_source s on bo.business_source_id=s.id "
			+ " <where>"
			+ " <if test='startIndex != null and startIndex!=\"\"' >"
			+ " 		AND rn > #{startIndex}"
			+ " </if>"
			+ " <if test='1==1' >"
			+ " 		AND bo.CHARGE_USER_ID is not null"
			+ " </if>"
			+ " </where> "
			+ " <choose>  "
            + "	<when test='CriteriaType==\"customer_name\"'>  "
           	+ "     AND customer_name  like '%'||#{content}||'%'  "
            + " </when>  "
            + "	<when test='CriteriaType==\"source_name\"'>  "
           	+ "     AND source_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"charge_user_name\"'>  "
           	+ "     AND u_charge.username like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"create_user_name\"'>  "
           	+ "     AND u_create.username like '%'||#{content}||'%' "
            + " </when>  "
            + "	</choose>  "
			+ "     order by bo.CREATE_TIME desc "
			+"</script>")
	List<BusinessOpportunityVo> pageList(@Param("startIndex") Integer startIndex, @Param("endIndex") 
		Integer endIndex,@Param("CriteriaType") String CriteriaType,@Param("content") String content);

	/**
	 * 获取总条数
	 * @return
	 */
	@Select("<script>"
			+ "SELECT count(bo.id) FROM ("
			+ "	SELECT A.*,ROWNUM rn "
			+ "		FROM ("
			+ " 	select * FROM BUSINESS_OPPORTUNITY "
			+ " 	<choose>  "
            + "			<when test='CriteriaType==\"business_name\"'>  "
           	+ "    			where business_name like '%'||#{content}||'%'  "
            + " 		</when>  "
            + "		</choose>  "
			+ "		) A" 
			+ "	  ) bo"
			+ "		left join sys_user u_charge on bo.charge_user_id=u_charge.id "
			+ "		left join sys_user u_create on bo.create_user_id=u_create.id "
			+ "		left join customer c on bo.customer_id=c.id "
			+ "		left join business_opportunity_source s on bo.business_source_id=s.id "
			+ " <where>"
			+ " bo.REMOVESTATE=0 "
			+ " <if test='1==1' >"
			+ " 		AND bo.CHARGE_USER_ID is not null"
			+ " </if>"
			+ " </where> "
			+ " <choose>  "
            + "	<when test='CriteriaType==\"customer_name\"'>  "
           	+ "     AND customer_name  like '%'||#{content}||'%'  "
            + " </when>  "
            + "	<when test='CriteriaType==\"source_name\"'>  "
           	+ "     AND source_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"charge_user_name\"'>  "
           	+ "     AND u_charge.username like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"create_user_name\"'>  "
           	+ "     AND u_create.username like '%'||#{content}||'%' "
            + " </when>  "
            + "	</choose>  "
			+ "     order by bo.CREATE_TIME desc "
			+"</script>")
	BigInteger totalCount(@Param("CriteriaType") String CriteriaType,@Param("content") String content);

	/**
	 * 条件分页查询
	 * @param beginIndex
	 * @param endIndex
	 * @param business_name
	 * @return
	 */
	@Select("<script>"
			+" select * from   (       "
			+ "  	select A.* ,ROWNUM rn"
			+ "		from ("
			+ "				select *  from BUSINESS_OPPORTUNITY"
			+ "				<choose>"
			+ "            	<when test='business_name != null and business_name!=\"\"'>"
			+ "					where business_name=#{business_name}"
			+ "            	</when> "
			+ "				</choose>"
			+ "				) A"
			+ "		<if test='endIndex != null and endIndex!=\"\"' >"
			+ " 			<![CDATA[where ROWNUM <=#{endIndex}]]>"
			+ "		</if>  )"
			+ " <if test='beginIndex != null and beginIndex!=\"\"' >"
			+ " 		Where rn > #{beginIndex}"
			+ " </if>"
			+"</script>")
	List<BusinessOpportunity> pageListWhere(@Param("beginIndex")Integer beginIndex, @Param("endIndex")Integer endIndex, @Param("business_name")String business_name);
	
	/**
	 * 获取商机总条数
	 * @return
	 */
	@Select("select count(id) from BUSINESS_OPPORTUNITY")
	Integer BusinessCount();
	
	/**
	 * 获取商机报表日期统计
	 * @param firstDayOfMonth
	 * @param lastDayOfMonth
	 * @return
	 */
	@Select("select t.CREATE_TIME createTime,count(t.id) numCount from BUSINESS_OPPORTUNITY t "
		+ "where t.REMOVESTATE=0 and t.CREATE_TIME >= to_date(#{firstDay},'yyyy-MM-dd') "
		+ "and t.CREATE_TIME <= to_date(#{lastDay},'yyyy-MM-dd') "
		+ "group by t.CREATE_TIME order by t.CREATE_TIME desc")
	List<BusinessCountVo> countMonth(@Param("firstDay") String firstDay
			,@Param("lastDay") String lastDay);
}
