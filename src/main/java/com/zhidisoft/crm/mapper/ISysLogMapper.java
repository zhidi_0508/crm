package com.zhidisoft.crm.mapper;

import java.math.BigInteger;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zhidisoft.crm.entity.SysLog;
import com.zhidisoft.crm.vo.SysLogVo;

public interface ISysLogMapper{

	@Insert("insert into sys_log(id,method_name,method_info,user_id)"
			+ " values(#{id},#{method_name},#{method_info},#{user_id})")
	public int save(SysLog log);
	
	@Select("select l.id,l.method_info,l.create_time,u.username from sys_log l left join sys_user u on l.user_id=u.id order by create_time desc limit #{startIndex},#{pageSize}")
	List<SysLogVo> pageList(
			@Param("startIndex") Integer startIndex,
			@Param("pageSize") Integer pageSize);
	
	/**
	 * 获取总条数
	 * @return
	 */
	@Select("SELECT count(id) FROM sys_log")
	public BigInteger totalCount();
}
