package com.zhidisoft.crm.mapper;

import java.math.BigInteger;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zhidisoft.crm.entity.SysFunction;

public interface ISysFunctionMapper {

	/**
	 * 新增
	 * @param sysFunction
	 * @return
	 */
	@Insert("insert into sys_function(id,func_code,func_name,func_type,func_url,parent_id)"
			+ " values(#{id},#{func_code},#{func_name},#{func_type,jdbcType=VARCHAR},#{func_url},#{parent_id})")
	int save(SysFunction sysFunction);

	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@Delete("delete from sys_function where id=#{id}")
	int deleteById(String id);

	/**
	 * 编辑
	 * @param func
	 * @return
	 */
	@Update("update sys_function set func_code=#{func_code},func_name=#{func_name},func_type=#{func_type,jdbcType=VARCHAR},func_url=#{func_url},parent_id=#{parent_id} where id=#{id}")
	int update(SysFunction func);

	/**
	 * 分页查询
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select f.id,f.func_code,f.func_name,f.func_type,f.func_url,f.parent_id,f.update_time from sys_function f"
			+ " order by update_time desc ) A"
			+ " WHERE ROWNUM <= #{endIndex})  TL "
			+ "WHERE RN > #{startIndex}")
	List<SysFunction> pageList(@Param("startIndex") Integer startIndex, @Param("endIndex") Integer endIndex);

	
	/**
	 * 获取总条数
	 * @return
	 */
	@Select("SELECT count(id) FROM sys_function")
	public BigInteger totalCount();

	/**
	 * 根据id查询
	 * @param id
	 * @return
	 */
	@Select("select * from sys_function where id=#{id}")
	SysFunction findById(String id);

	/**
	 * 根据func_name查询
	 * @param func_name
	 * @return
	 */
	@Select("select * from sys_function where func_name=#{func_name}")
	SysFunction findByFunc(String func_name);
	
	/**
	 * 根据parent_id查询
	 * @param func_name
	 * @return
	 */
	@Select("select * from sys_function where parent_id=#{parent_id}")
	SysFunction findByParent(String func_name);
	
	/**
	 * 查询所有菜单
	 * @return
	 */
	@Select("select * from sys_function")
	List<SysFunction> findFuncAll();

	/**
	 * 根据用户ID获取菜单
	 * @param username
	 * @return
	 */
	@Select("select f.* from sys_function f "
			+ "inner join sys_role_func rf on f.id=rf.func_id "
			+ "inner join sys_role r on rf.role_id=r.id "
			+ "inner join sys_user_role ur on ur.role_id=r.id "
			+ "inner join sys_user u on ur.user_id=u.id "
			+ "where u.id=#{userId}")
	List<SysFunction> findByUser(String userId);

}
