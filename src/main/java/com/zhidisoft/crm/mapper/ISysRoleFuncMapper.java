package com.zhidisoft.crm.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;

import com.zhidisoft.crm.entity.SysRoleFunc;

public interface ISysRoleFuncMapper {

	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@Delete("delete from sys_role_func where role_id=#{roleId}")
	int deleteById(String roleId);

	/**
	 * 添加
	 * @param roleFunc
	 */
	@Insert("insert into sys_role_func(id,role_id,func_id) values(#{id},#{role_id},#{func_id})")
	void save(SysRoleFunc roleFunc);
}
