package com.zhidisoft.crm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.zhidisoft.crm.entity.BusinessOpportunity;
import com.zhidisoft.crm.entity.Customer;
import com.zhidisoft.crm.entity.Business_opportunity_source;
import com.zhidisoft.crm.entity.Business_opportunity_status;
import com.zhidisoft.crm.entity.Business_opportunity_type;

public interface IBussinessMapper {

	/**
	 * 查询商机全部类型
	 * @return
	 */
	@Select("select * from business_opportunity_type")
	List<Business_opportunity_type> findTypeAll();

	/**
	 * 查询商机全部状态
	 * @return
	 */
	@Select("select * from business_opportunity_status")
	List<Business_opportunity_status> findStatusAll();
	
	@Select("select t.* from BUSINESS_OPPORTUNITY_STATUS t where "
			+ "t.id=(select o.BUSINESS_STATUS_ID from Business_Opportunity o "
			+ "where o.id=#{id})")
	Business_opportunity_status	findStatus(String id);
	/**
	 * 查询商机全部来源
	 * @return
	 */
	@Select("select * from business_opportunity_source")
	List<Business_opportunity_source> findSourceAll();
	
	/**
	 * 查询所有客户
	 * @return
	 */
	@Select("select * from customer")
	List<Customer> findCustomerAll();

	/**
	 * 获取所有商机
	 * @return
	 */
	@Select("select * from business_opportunity")
	List<BusinessOpportunity> findBusinessAll();

}
