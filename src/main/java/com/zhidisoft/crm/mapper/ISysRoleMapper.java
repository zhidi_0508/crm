package com.zhidisoft.crm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zhidisoft.crm.entity.SysRole;
/**
 * 角色管理
 * @author Administrator
 *
 */
public interface ISysRoleMapper {

	/**
	 * 新增角色
	 * @param sysRole
	 */
	@Insert("insert into sys_role(id,role_name,role_info) values(#{id},#{role_name},#{role_info})")
	int save(SysRole sysRole);

	/**
	 * 删除角色
	 * @param id
	 * @return
	 */
	@Delete("delete from sys_role where id = #{id}")
	public int deleteById(String id);
	
	
	/**
	 * 根据id获取用户
	 * @param id
	 * @return
	 */
	@Select("select role_name,role_info from sys_role where id=#{id}")
	SysRole findById(String id);
	
	/**
	 * 分页查询
	 * @param startIndex
	 * @param pageSize
	 * @return
	 */
//	@Select("select id,role_name,role_info from sys_role limit #{startIndex},#{pageSize}")
//	List<SysRole> pageList(@Param("startIndex") Integer startIndex, @Param("pageSize") Integer pageSize);

	/**
	 * 分页查询
	 * oracle查询
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select id,role_name,role_info from sys_role ) A"
			+ " WHERE ROWNUM <= #{endIndex})  TL "
			+ "WHERE RN > #{startIndex}")
	List<SysRole> pageList(@Param("startIndex") Integer startIndex, @Param("endIndex") Integer endIndex);
	
	/**
	 * 获取总条数
	 * @return
	 */
	@Select("SELECT count(id) FROM sys_role")
	Long totalCount();

	/**
	 * 修改角色
	 * @param sysRole
	 * @return
	 */
	@Update("update sys_role set role_name=#{role_name},role_info=#{role_info} where id=#{id}")
	int update(SysRole sysRole);
	

	
	/**
	 * 查询所有角色
	 * @return
	 */
	@Select("select * from sys_role")
	List<SysRole> select();
	
	/**
	 * 根据用户名获取角色
	 * @param username
	 * @return
	 */
	@Select("select r.* from sys_role r "
			+ "inner join sys_user_role ur on ur.role_id=r.id "
			+ "inner join sys_user u on ur.user_id=u.id "
			+ "where u.id=#{userId}")
	List<SysRole> getRole(String userId);
	
}
