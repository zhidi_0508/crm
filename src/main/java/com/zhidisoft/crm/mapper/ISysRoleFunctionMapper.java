package com.zhidisoft.crm.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;

import com.zhidisoft.crm.entity.SysRoleFunc;

public interface ISysRoleFunctionMapper {
	@Insert("insert into sys_role_func(id,role_id,func_id) values(#{id},#{role_id},#{func_id})")
	 void save(SysRoleFunc roleFunc);
	
	@Delete("delete from sys_role_func where role_id=#{roleId}")
	void delete(String roleId);
}
