package com.zhidisoft.crm.mapper;

import java.math.BigInteger;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.zhidisoft.crm.entity.Contract;
import com.zhidisoft.crm.entity.Contract_status;
import com.zhidisoft.crm.vo.ContractCountVo;
import com.zhidisoft.crm.vo.ContractListVo;

public interface IContractMapper {
	
	/**
	 * 添加合同
	 * @param contractVo
	 * @return
	 */
	@Insert("insert into CONTRACT(CONTRACT_NO,CUSTOMER_ID,CONTRACT_STATUS_ID,DATE_OF_CONTRACT,BUSINESS_OPPORTUNITY_ID,CONTACT_NAME"
			+ ",CHARGE_USER_ID,CONTRACT_MONEY,EFFECTIVE_DATE_OF_CONTRACT,CONTRACT_EXPIRY_DATE"
			+ ",TERMS_AND_CONDITIONS,THE_CONTRACT_DESCRIPTION)"
			+ " values (#{CONTRACT_NO},#{CUSTOMER_ID,jdbcType=VARCHAR},#{CONTRACT_STATUS_ID,jdbcType=VARCHAR},#{DATE_OF_CONTRACT},#{BUSINESS_OPPORTUNITY_ID,jdbcType=VARCHAR},#{CONTACT_NAME}"
			+ ",#{CHARGE_USER_ID},#{CONTRACT_MONEY},#{EFFECTIVE_DATE_OF_CONTRACT},#{CONTRACT_EXPIRY_DATE}"
			+ ",#{TERMS_AND_CONDITIONS,jdbcType=BLOB},#{THE_CONTRACT_DESCRIPTION})")
	int save(Contract contract);
	
	/**
	 * 修改合同
	 * @param contract
	 * @return
	 */																																								
	@Update("update CONTRACT set CONTRACT_NO=#{CONTRACT_NO},CUSTOMER_ID=#{CUSTOMER_ID},CONTRACT_STATUS_ID=#{CONTRACT_STATUS_ID},DATE_OF_CONTRACT=#{DATE_OF_CONTRACT},BUSINESS_OPPORTUNITY_ID=#{BUSINESS_OPPORTUNITY_ID}"
			+ ",CONTACT_NAME=#{CONTACT_NAME},CHARGE_USER_ID=#{CHARGE_USER_ID},CONTRACT_MONEY=#{CONTRACT_MONEY},EFFECTIVE_DATE_OF_CONTRACT=#{EFFECTIVE_DATE_OF_CONTRACT}"
			+ ",CONTRACT_EXPIRY_DATE=#{CONTRACT_EXPIRY_DATE},TERMS_AND_CONDITIONS=#{TERMS_AND_CONDITIONS},THE_CONTRACT_DESCRIPTION=#{THE_CONTRACT_DESCRIPTION} where ID=#{id}")
	int update(Contract contract);
	
	/**
	 * 批量删除对象
	 * @param ids
	 */
	@Delete("update contract set removeState=1 where id=#{ids}")
	void deleteAll(String ids);
	
	/**
	 * 分页查询
	 * @param beginIndex
	 * @param endIndex
	 * @return
	 */
	@Select("<script>"
			+ "SELECT cu.*,c.customer_name,u.username,s.status_name FROM ("
			+ "	SELECT A.*,ROWNUM rn "
			+ "		FROM ("
			+ " 	select * FROM CONTRACT "
			+ " 	<choose>  "
            + "			<when test='CriteriaType==\"contact_name\"'>  "
           	+ "    			where contact_name like '%'||#{content}||'%'  "
            + " 		</when>  "
            + "		</choose>  "
			+ "		) A" 
			+ "	 <if test='endIndex != null and endIndex!=\"\"' >"
			+ " 	<![CDATA[where ROWNUM <=#{endIndex} and REMOVESTATE=0]]>"
			+ "	 </if>  ) cu"
			+ " 	left join CUSTOMER C on cu.CUSTOMER_ID = c.id "
			+ " 	left join SYS_USER u on cu.CHARGE_USER_ID = u.id "
			+ " 	left join CONTRACT_STATUS s on cu.CONTRACT_STATUS_ID = s.id "
			+ " <where>"
			+ " <if test='startIndex != null and startIndex!=\"\"' >"
			+ " 		AND rn > #{startIndex}"
			+ " </if>"
			+ " <if test='1==1' >"
			+ " 		AND CUSTOMER_ID is not null"
			+ " </if>"
			+ " </where> "
			+ " <choose>  "
            + "	<when test='CriteriaType==\"customer_name\"'>  "
           	+ "     AND customer_name  like '%'||#{content}||'%'  "
            + " </when>  "
            + "	<when test='CriteriaType==\"username\"'>  "
           	+ "     AND username like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"status_name\"'>  "
           	+ "     AND status_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"status_name\"'>  "
           	+ "     AND status_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	</choose>  "
			+ "     order by CONTRACT_NO desc "
			+"</script>")
	List<ContractListVo> pageListWhere(@Param("startIndex") Integer startIndex, @Param("endIndex") 
		Integer endIndex,@Param("CriteriaType") String CriteriaType,@Param("content") String content);

	/**
	 * 获取总条数
	 * @return
	 */
	@Select("<script>"
			+ "SELECT count(cu.id) FROM ("
			+ "	SELECT A.*,ROWNUM rn "
			+ "		FROM ("
			+ " 	select * FROM CONTRACT "
			+ " 	<choose>  "
            + "			<when test='CriteriaType==\"contact_name\"'>  "
           	+ "    			where contact_name like '%'||#{content}||'%'  "
            + " 		</when>  "
            + "		</choose>  "
			+ "		) A" 
			+ "	  ) cu"
			+ " 	left join CUSTOMER C on cu.CUSTOMER_ID = c.id "
			+ " 	left join SYS_USER u on cu.CHARGE_USER_ID = u.id "
			+ " 	left join CONTRACT_STATUS s on cu.CONTRACT_STATUS_ID = s.id "
			+ " <where>"
			+ " cu.REMOVESTATE=0 "
			+ " <if test='1==1' >"
			+ " 		AND CUSTOMER_ID is not null"
			+ " </if>"
			+ " </where> "
			+ " <choose>  "
            + "	<when test='CriteriaType==\"customer_name\"'>  "
           	+ "     AND customer_name  like '%'||#{content}||'%'  "
            + " </when>  "
            + "	<when test='CriteriaType==\"username\"'>  "
           	+ "     AND username like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"status_name\"'>  "
           	+ "     AND status_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"status_name\"'>  "
           	+ "     AND status_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	</choose>  "
			+ "     order by CONTRACT_NO desc "
			+"</script>")
	BigInteger totalCount(@Param("CriteriaType") String CriteriaType,@Param("content") String content);
	
	/**
	 * 获取合同总条数
	 * @return
	 */
	@Select("select count(id) from contract")
	Integer ContractCount();
	
	/**
	 * 根据id 获取数据
	 * @param id
	 * @return
	 */
	@Select("select * from contract where id=#{id}")
	Contract findByContract(String id);
	
	/**
	 * 获取所有合同
	 * @return
	 */
	@Select("select * from contract_status")
	List<Contract_status> findStatusAll();
	
	/**
	 * 查询合同详情
	 * @param id
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select t.*,c.customer_name,u.username,s.status_name,o.business_name from CONTRACT t "
			+ "left join CUSTOMER c on t.customer_id=c.id "
			+ "left join Sys_User u on t.charge_user_id=u.id "
			+ "left join CONTRACT_STATUS s on t.contract_status_id=s.id "
			+ "left join BUSINESS_OPPORTUNITY o on t.business_opportunity_id=o.id) A"
			+ ") WHERE ID = #{id}")
	ContractListVo findByid(String id);

	/**
	 * 获取合同一周报表统计
	 * @param firstDayOfWeek
	 * @param lastDayOfWeek
	 * @return
	 */
	@Select("select t.Date_Of_Contract dateOfContract,count(t.id) numCount from CONTRACT t "
		+ "where t.REMOVESTATE=0 and t.Date_Of_Contract >= to_date(#{firstTime},'yyyy-MM-dd') "
		+ "and t.Date_Of_Contract <=  to_date(#{lastTime},'yyyy-MM-dd') "
		+ "group by t.Date_Of_Contract order by t.Date_Of_Contract")
	public List<ContractCountVo> contractCount(@Param("firstTime") String firstTime,
			@Param("lastTime") String lastTime);
	
}
