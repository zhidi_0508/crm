package com.zhidisoft.crm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zhidisoft.crm.entity.BusinessBoost;
import com.zhidisoft.crm.vo.BusinessBoostListVo;

public interface IBusinessBoostMapper {

	/**
	 * 添加推进历史
	 * @param boost
	 * @return
	 */
	@Insert("insert into BUSINESS_BOOST(BUSINESS_STATUS_ID,NEXT_CONTACT_TIME,NEXT_CONTACT_INFO,DESCRIPTION,USER_ID)"
			+ " values(#{BUSINESS_STATUS_ID},#{NEXT_CONTACT_TIME},#{NEXT_CONTACT_INFO,jdbcType=BLOB},#{DESCRIPTION,jdbcType=BLOB},#{USER_ID})")
	int add(BusinessBoost boost);

	/**
	 * 推进历史分页列表
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select b.*,bo.status_name,s.username from business_boost b"
			+ " left join business_opportunity_status bo on b.business_status_id=bo.id"
			+ " left join sys_user s on b.user_id=s.id) A"
			+ " WHERE ROWNUM <= #{endIndex})  TL "
			+ "WHERE RN > #{startIndex}")
	List<BusinessBoostListVo> pageList(@Param("startIndex") Integer startIndex, @Param("endIndex") Integer endIndex);

	/**
	 * 查询推进历史总条数
	 * @return	
	 */
	@Select("SELECT count(id) FROM business_boost")
	Long totalCount();

	/**
	 * 查询推进历史详情
	 * @param id
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select b.*,bo.status_name,s.username from business_boost b "
			+ " left join business_opportunity_status bo on b.business_status_id=bo.id"
			+ " left join sys_user s on b.user_id=s.id) A"
			+ ") WHERE ID = #{id}")
	BusinessBoostListVo findById(String id);
}
