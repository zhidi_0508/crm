package com.zhidisoft.crm.mapper;

import java.math.BigInteger;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zhidisoft.crm.vo.BusinessOpportunityVo;

public interface IBusinessRecycleBinMapper {

	/**
	 * 恢复商机
	 * @param ids
	 */
	@Delete("update BUSINESS_OPPORTUNITY set removeState=0 where id=#{ids}")
	void recoverAll(String ids);

	/**
	 * 根据id查询数据(详情)
	 * @param id
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select t.*,u_charge.username charge_user_name,u_create.username create_user_name"
			+ ",c.customer_name,s.source_name,o.type_name,bos.status_name from BUSINESS_OPPORTUNITY t "
			+ "left join sys_user u_charge on t.charge_user_id=u_charge.id "
			+ "left join sys_user u_create on t.create_user_id=u_create.id "
			+ "left join customer c on t.customer_id=c.id "
			+ "left join business_opportunity_status bos on t.business_status_id=bos.id "
			+ "left join business_opportunity_source s on t.business_source_id=s.id "
			+ "left join business_opportunity_type o on t.business_type_id=o.id ) A"
			+ ") WHERE ID = #{id}")
	BusinessOpportunityVo findById(String id);
	
	/**
	 * 获取所有商机
	 * @return
	 */
	@Select("select * from BUSINESS_OPPORTUNITY")
	List<BusinessOpportunityVo> findBusinessAll();
	
	/**
	 * 分页查询
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	@Select("<script>"
			+ "SELECT bo.*,u_charge.username charge_user_name,u_create.username create_user_name"
			+ ",c.customer_name,s.source_name FROM ("
			+ "	SELECT A.*,ROWNUM rn "
			+ "		FROM ("
			+ " 	select * FROM BUSINESS_OPPORTUNITY "
			+ " 	<choose>  "
            + "			<when test='CriteriaType==\"business_name\"'>  "
           	+ "    			where business_name like '%'||#{content}||'%'  "
            + " 		</when>  "
            + "		</choose>  "
			+ "		) A" 
			+ "	 <if test='endIndex != null and endIndex!=\"\"' >"
			+ " 	<![CDATA[where ROWNUM <=#{endIndex} and REMOVESTATE=1]]>"
			+ "	 </if>  ) bo"
			+ "		left join sys_user u_charge on bo.charge_user_id=u_charge.id "
			+ "		left join sys_user u_create on bo.create_user_id=u_create.id "
			+ "		left join customer c on bo.customer_id=c.id "
			+ "		left join business_opportunity_source s on bo.business_source_id=s.id "
			+ " <where>"
			+ " <if test='startIndex != null and startIndex!=\"\"' >"
			+ " 		AND rn > #{startIndex}"
			+ " </if>"
			+ " <if test='1==1' >"
			+ " 		AND bo.CHARGE_USER_ID is not null"
			+ " </if>"
			+ " </where> "
			+ " <choose>  "
            + "	<when test='CriteriaType==\"customer_name\"'>  "
           	+ "     AND customer_name  like '%'||#{content}||'%'  "
            + " </when>  "
            + "	<when test='CriteriaType==\"source_name\"'>  "
           	+ "     AND source_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"charge_user_name\"'>  "
           	+ "     AND u_charge.username like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"create_user_name\"'>  "
           	+ "     AND u_create.username like '%'||#{content}||'%' "
            + " </when>  "
            + "	</choose>  "
			+ "     order by bo.CREATE_TIME desc "
			+"</script>")
	List<BusinessOpportunityVo> pageList(@Param("startIndex") Integer startIndex, @Param("endIndex") 
		Integer endIndex,@Param("CriteriaType") String CriteriaType,@Param("content") String content);

	/**
	 * 获取总条数
	 * @return
	 */
	@Select("<script>"
			+ "SELECT count(bo.id) FROM ("
			+ "	SELECT A.*,ROWNUM rn "
			+ "		FROM ("
			+ " 	select * FROM BUSINESS_OPPORTUNITY "
			+ " 	<choose>  "
            + "			<when test='CriteriaType==\"business_name\"'>  "
           	+ "    			where business_name like '%'||#{content}||'%'  "
            + " 		</when>  "
            + "		</choose>  "
			+ "		) A" 
			+ "	  ) bo"
			+ "		left join sys_user u_charge on bo.charge_user_id=u_charge.id "
			+ "		left join sys_user u_create on bo.create_user_id=u_create.id "
			+ "		left join customer c on bo.customer_id=c.id "
			+ "		left join business_opportunity_source s on bo.business_source_id=s.id "
			+ " <where>"
			+ " bo.REMOVESTATE=1 "
			+ " <if test='1==1' >"
			+ " 		AND bo.CHARGE_USER_ID is not null"
			+ " </if>"
			+ " </where> "
			+ " <choose>  "
            + "	<when test='CriteriaType==\"customer_name\"'>  "
           	+ "     AND customer_name  like '%'||#{content}||'%'  "
            + " </when>  "
            + "	<when test='CriteriaType==\"source_name\"'>  "
           	+ "     AND source_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"charge_user_name\"'>  "
           	+ "     AND u_charge.username like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"create_user_name\"'>  "
           	+ "     AND u_create.username like '%'||#{content}||'%' "
            + " </when>  "
            + "	</choose>  "
			+ "     order by bo.CREATE_TIME desc "
			+"</script>")
	BigInteger totalCount(@Param("CriteriaType") String CriteriaType,@Param("content") String content);

}
