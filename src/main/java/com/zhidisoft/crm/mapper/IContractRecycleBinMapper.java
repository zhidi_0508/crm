package com.zhidisoft.crm.mapper;

import java.math.BigInteger;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.zhidisoft.crm.vo.ContractListVo;

public interface IContractRecycleBinMapper {
	
	/**
	 * 批量恢复合同
	 * @param ids
	 */
	@Delete("update contract set removeState=0 where id=#{ids}")
	void recoverAll(String ids);
	
	/**
	 * 分页查询
	 * @param beginIndex
	 * @param endIndex
	 * @return
	 */
	@Select("<script>"
			+ "SELECT cu.*,c.customer_name,u.username,s.status_name FROM ("
			+ "	SELECT A.*,ROWNUM rn "
			+ "		FROM ("
			+ " 	select * FROM CONTRACT "
			+ " 	<choose>  "
            + "			<when test='CriteriaType==\"contact_name\"'>  "
           	+ "    			where contact_name like '%'||#{content}||'%'  "
            + " 		</when>  "
            + "		</choose>  "
			+ "		) A" 
			+ "	 <if test='endIndex != null and endIndex!=\"\"' >"
			+ " 	<![CDATA[where ROWNUM <=#{endIndex} and REMOVESTATE=1]]>"
			+ "	 </if>  ) cu"
			+ " 	left join CUSTOMER C on cu.CUSTOMER_ID = c.id "
			+ " 	left join SYS_USER u on cu.CHARGE_USER_ID = u.id "
			+ " 	left join CONTRACT_STATUS s on cu.CONTRACT_STATUS_ID = s.id "
			+ " <where>"
			+ " <if test='startIndex != null and startIndex!=\"\"' >"
			+ " 		AND rn > #{startIndex}"
			+ " </if>"
			+ " <if test='1==1' >"
			+ " 		AND CUSTOMER_ID is not null"
			+ " </if>"
			+ " </where> "
			+ " <choose>  "
            + "	<when test='CriteriaType==\"customer_name\"'>  "
           	+ "     AND customer_name  like '%'||#{content}||'%'  "
            + " </when>  "
            + "	<when test='CriteriaType==\"username\"'>  "
           	+ "     AND username like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"status_name\"'>  "
           	+ "     AND status_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"status_name\"'>  "
           	+ "     AND status_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	</choose>  "
			+ "     order by CONTRACT_NO desc "
			+"</script>")
	List<ContractListVo> pageList(@Param("startIndex") Integer startIndex, @Param("endIndex") 
		Integer endIndex,@Param("CriteriaType") String CriteriaType,@Param("content") String content);

	/**
	 * 获取总条数
	 * @return
	 */
	@Select("<script>"
			+ "SELECT count(cu.id) FROM ("
			+ "	SELECT A.*,ROWNUM rn "
			+ "		FROM ("
			+ " 	select * FROM CONTRACT "
			+ " 	<choose>  "
            + "			<when test='CriteriaType==\"contact_name\"'>  "
           	+ "    			where contact_name like '%'||#{content}||'%'  "
            + " 		</when>  "
            + "		</choose>  "
			+ "		) A" 
			+ "	  ) cu"
			+ " 	left join CUSTOMER C on cu.CUSTOMER_ID = c.id "
			+ " 	left join SYS_USER u on cu.CHARGE_USER_ID = u.id "
			+ " 	left join CONTRACT_STATUS s on cu.CONTRACT_STATUS_ID = s.id "
			+ " <where>"
			+ " cu.REMOVESTATE=1 "
			+ " <if test='1==1' >"
			+ " 		AND CUSTOMER_ID is not null"
			+ " </if>"
			+ " </where> "
			+ " <choose>  "
            + "	<when test='CriteriaType==\"customer_name\"'>  "
           	+ "     AND customer_name  like '%'||#{content}||'%'  "
            + " </when>  "
            + "	<when test='CriteriaType==\"username\"'>  "
           	+ "     AND username like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"status_name\"'>  "
           	+ "     AND status_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	<when test='CriteriaType==\"status_name\"'>  "
           	+ "     AND status_name like '%'||#{content}||'%' "
            + " </when>  "
            + "	</choose>  "
			+ "     order by CONTRACT_NO desc "
			+"</script>")
	BigInteger totalCount(@Param("CriteriaType") String CriteriaType,@Param("content") String content);
	
	/**
	 * 查询合同详情
	 * @param id
	 * @return
	 */
	@Select("SELECT * FROM("
			+ "SELECT A.*,ROWNUM RN "
			+ "FROM "
			+ " (select t.*,c.customer_name,u.username,s.status_name,o.business_name from CONTRACT t "
			+ "left join CUSTOMER c on t.customer_id=c.id "
			+ "left join Sys_User u on t.charge_user_id=u.id "
			+ "left join CONTRACT_STATUS s on t.contract_status_id=s.id "
			+ "left join BUSINESS_OPPORTUNITY o on t.business_opportunity_id=o.id) A"
			+ ")WHERE ID = #{id}")
	ContractListVo findByid(String id);

}
