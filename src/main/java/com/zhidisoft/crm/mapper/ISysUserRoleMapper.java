package com.zhidisoft.crm.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import com.zhidisoft.crm.entity.SysUserRole;

public interface ISysUserRoleMapper {
	/**
	 * 添加
	 * @param userRole
	 */
	@Insert("insert into sys_user_role(id,user_id,role_id) values(#{id},#{user_id},#{role_id})")
	void save(SysUserRole userRole);
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@Delete("delete from sys_user_role where user_id=#{userId}")
	int deleteById(String userId);

	/**
	 * 查询用户角色
	 * @param userId
	 * @return
	 */
	@Select("select * from sys_user_role where user_id=#{userId}")
	SysUserRole findById(String userId);
}
