package com.zhidisoft.crm.service;

import java.util.List;

import com.zhidisoft.crm.entity.BusinessOpportunity;
import com.zhidisoft.crm.entity.Customer;
import com.zhidisoft.crm.entity.Business_opportunity_source;
import com.zhidisoft.crm.entity.Business_opportunity_status;
import com.zhidisoft.crm.entity.Business_opportunity_type;

public interface IBussinessService {

	List<Business_opportunity_type> findTypeAll();

	List<Business_opportunity_status> findStatusAll();

	List<Business_opportunity_source> findSourceAll();

	List<Customer> findCustomerAll();
	
	List<BusinessOpportunity> findBusinessAll();

	Business_opportunity_status findStatus(String id);
	
}
