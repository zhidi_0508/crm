package com.zhidisoft.crm.service;

import java.util.List;

import com.zhidisoft.crm.entity.Contract;
import com.zhidisoft.crm.entity.Contract_status;
import com.zhidisoft.crm.vo.ContractCountVo;
import com.zhidisoft.crm.vo.ContractListVo;
import com.zhidisoft.crm.vo.PageVo;

public interface IContractService {

	int save(Contract contract);
	
	void deleteAll(String string);

	int update (Contract contract);
	
	ContractListVo findByid(String id);
	
	Contract findByContract(String id);
	
	PageVo findPage(Integer pageNum, Integer pageSize, String CriteriaType,String content);
	
	List<Contract_status> findStatusAll();

	List<ContractCountVo> contractCount(String firstTime, String lastTime);

	Integer ContractCount();
}
