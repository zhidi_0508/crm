package com.zhidisoft.crm.service;

public interface ISysRoleFuncService {

	/**
	 * 给角色分配菜单(添加)
	 * @param roleId
	 * @param funcIds
	 */
	void save(String roleId, String[] funcIds);

}
