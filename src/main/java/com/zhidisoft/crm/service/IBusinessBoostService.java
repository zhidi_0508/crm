package com.zhidisoft.crm.service;

import com.zhidisoft.crm.entity.BusinessBoost;
import com.zhidisoft.crm.vo.BusinessBoostListVo;
import com.zhidisoft.crm.vo.PageVo;

public interface IBusinessBoostService {

	int add(BusinessBoost boost);

	PageVo findPage(Integer pageNum, Integer pageSize);

	BusinessBoostListVo findById(String id);
}
