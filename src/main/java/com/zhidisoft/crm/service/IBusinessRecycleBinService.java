package com.zhidisoft.crm.service;

import java.util.List;

import com.zhidisoft.crm.vo.BusinessOpportunityVo;
import com.zhidisoft.crm.vo.PageVo;

public interface IBusinessRecycleBinService {

	BusinessOpportunityVo findById(String id);
	
	PageVo findPage(Integer pageNum, Integer pageSize,String CriteriaType,String content);

	List<BusinessOpportunityVo> findBusinessAll();

	void recoverAll(String ids);
}
