package com.zhidisoft.crm.service;

import java.util.List;

import com.zhidisoft.crm.entity.SysRole;
import com.zhidisoft.crm.vo.PageVo;

public interface ISysRoleService {
	

	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageVo findPage(Integer pageNum, Integer pageSize);

	/**
	 * 根据ID查询用户
	 * @param id
	 * @return
	 */
	SysRole findById(String id);

	/**
	 * 根据姓名查询用户
	 * @param username
	 * @return
	 */
	/*public SysRole findByUserName(String username);
*/
	/**
	 * 
	 * @param sysUser
	 * @return
	 */
	int save(SysRole sysRole);

	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public int deleteRole(String id);

	/**
	 * 修改
	 * @param sysUser
	 */
	public int update(SysRole sysRole);
    /**
     * 查询全部
     * @return
     */
	public List<SysRole> showSysrole();
	
	}

