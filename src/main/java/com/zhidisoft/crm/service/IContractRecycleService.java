package com.zhidisoft.crm.service;

import com.zhidisoft.crm.vo.ContractListVo;
import com.zhidisoft.crm.vo.PageVo;

public interface IContractRecycleService {

	PageVo findPage(Integer pageNum, Integer pageSize,String CriteriaType,String content);

	ContractListVo findByid(String id);

	void recoverAll(String ids);
}
