package com.zhidisoft.crm.service;

import java.util.List;

import com.zhidisoft.crm.entity.SysUser;
import com.zhidisoft.crm.vo.PageVo;

public interface ISysUserService {

	/**
	 * 分页查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageVo findPage(Integer pageNum, Integer pageSize);

	/**
	 * 按条件分页查询
	 * @param pageNum
	 * @param pageSize
	 * @param username
	 * @return
	 */
	public PageVo findPage(Integer pageNum, Integer pageSize, String username);
	
	/**
	 * 根据ID查询用户
	 * @param id
	 * @return
	 */
	SysUser findById(String id);

	/**
	 * 根据姓名查询用户
	 * @param username
	 * @return
	 */
	public SysUser findByUserName(String username);

	/**
	 * 添加
	 * @param sysUser
	 * @return
	 */
	int save(SysUser sysUser);

	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public int deleteById(String id);

	/**
	 * 修改
	 * @param sysUser
	 */
	public int update(SysUser sysUser);

	/**
	 * 查询全部用户
	 * @return
	 */
	public List<SysUser> findUserAll();

	public SysUser getUser(String charge_username);

	
	
}
