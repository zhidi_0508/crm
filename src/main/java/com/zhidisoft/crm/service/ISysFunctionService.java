package com.zhidisoft.crm.service;

import java.util.List;

import com.zhidisoft.crm.entity.SysFunction;
import com.zhidisoft.crm.vo.PageVo;

public interface ISysFunctionService {

	public PageVo findPage(Integer pageNum, Integer pageSize);

	SysFunction findById(String id);

	int save(SysFunction sysFunction);

	public int deleteById(String id);
	
	int update(SysFunction func);
	
	SysFunction findByFunc(String func_name);
	
	SysFunction findByParent(String parent_id);

	List<SysFunction> findByUser(String id);

	List<SysFunction> findFuncAll();

}
