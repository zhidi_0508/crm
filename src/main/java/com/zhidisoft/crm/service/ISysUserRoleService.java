package com.zhidisoft.crm.service;

import com.zhidisoft.crm.entity.SysUserRole;

public interface ISysUserRoleService {
	/**
	 * 给用户分配角色
	 */
	public void saveUserRole(SysUserRole sysUserRole) ;

	/**
	 * 查询用户角色
	 * @param userId
	 * @return
	 */
	public SysUserRole findById(String userId);

}
