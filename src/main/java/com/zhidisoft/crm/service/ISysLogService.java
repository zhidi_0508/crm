package com.zhidisoft.crm.service;

import com.zhidisoft.crm.vo.PageVo;

public interface ISysLogService {

	PageVo findPage(Integer pageNum, Integer pageSize);
}
