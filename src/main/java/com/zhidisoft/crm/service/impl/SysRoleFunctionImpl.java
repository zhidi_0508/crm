package com.zhidisoft.crm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.anno.ServiceLogAnno;
import com.zhidisoft.crm.entity.SysRoleFunc;
import com.zhidisoft.crm.mapper.ISysRoleFunctionMapper;
import com.zhidisoft.crm.service.ISysRoleFunctionService;

@Service
@Transactional
public class SysRoleFunctionImpl implements ISysRoleFunctionService {

	@Autowired
	ISysRoleFunctionMapper roleFunctionMapper;

	@Override
	@ServiceLogAnno("角色菜单-添加")
	public void save(String roleId,String[] funcIds) {
		//先删除已存在的role菜单
		roleFunctionMapper.delete(roleId);
		//在for循环存入新的role菜单
		SysRoleFunc roleFunc;
		for (String funcId : funcIds) {
			roleFunc = new SysRoleFunc();
			roleFunc.setId(funcId);
			roleFunc.setRole_id(roleId);
			roleFunc.setFunc_id(funcId);
			roleFunctionMapper.save(roleFunc);
		}
		
	}
}
