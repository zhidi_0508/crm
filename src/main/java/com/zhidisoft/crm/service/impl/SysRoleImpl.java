package com.zhidisoft.crm.service.impl;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.anno.ServiceLogAnno;
import com.zhidisoft.crm.entity.SysRole;
import com.zhidisoft.crm.mapper.ISysRoleMapper;
import com.zhidisoft.crm.service.ISysRoleService;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class SysRoleImpl implements ISysRoleService{

	
	@Autowired
	ISysRoleMapper dao;
	/**
	 * 分页
	 */
	@Override
	@ServiceLogAnno("角色-分页查询")
	public PageVo findPage(Integer pageNum, Integer pageSize) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(dao.pageList(beginIndex, endIndex));
		pageVo.setTotalCount(dao.totalCount().intValue());
		return pageVo;
	}

	@Override
	public SysRole findById(String id) {
		
		return dao.findById(id);
	}

	/**
	 * 添加角色
	 */
	@Override
	@ServiceLogAnno("角色-添加")
	public int save(SysRole sysRole) {
		return dao.save(sysRole);
	}
	/**
	 * 删除角色
	 */
	@Override
	@ServiceLogAnno("角色-删除")
	public int deleteRole(String id) {
		return dao.deleteById(id);
	}
	/**
	 * 更新角色
	 */
	@Override
	@ServiceLogAnno("角色-编辑")
	public int update(SysRole sysRole) {
		return dao.update(sysRole);
	}

	
	/**
	 * 查询所有角色
	 */
	@Override
	@ServiceLogAnno("角色-查询所有")
	public List<SysRole> showSysrole() {
		return dao.select();
	}

}
