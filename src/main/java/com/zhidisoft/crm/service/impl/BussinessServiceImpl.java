package com.zhidisoft.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.entity.BusinessOpportunity;
import com.zhidisoft.crm.entity.Customer;
import com.zhidisoft.crm.entity.Business_opportunity_source;
import com.zhidisoft.crm.entity.Business_opportunity_status;
import com.zhidisoft.crm.entity.Business_opportunity_type;
import com.zhidisoft.crm.mapper.IBussinessMapper;
import com.zhidisoft.crm.service.IBussinessService;

@Service
@Transactional
public class BussinessServiceImpl implements IBussinessService{

	@Autowired
	IBussinessMapper mapper;
	
	@Override
	public List<Business_opportunity_type> findTypeAll() {
		return mapper.findTypeAll();
	}

	@Override
	public List<Business_opportunity_status> findStatusAll() {
		return mapper.findStatusAll();
	}

	@Override
	public List<Business_opportunity_source> findSourceAll() {
		return mapper.findSourceAll();
	}

	@Override
	public List<Customer> findCustomerAll() {
		return mapper.findCustomerAll();
	}
	
	@Override
	public List<BusinessOpportunity> findBusinessAll() {
		return mapper.findBusinessAll();
	}

	@Override
	public Business_opportunity_status findStatus(String id) {
		return mapper.findStatus(id);
	}

}
