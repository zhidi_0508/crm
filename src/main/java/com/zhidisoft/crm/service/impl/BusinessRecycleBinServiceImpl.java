package com.zhidisoft.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.mapper.IBusinessRecycleBinMapper;
import com.zhidisoft.crm.service.IBusinessRecycleBinService;
import com.zhidisoft.crm.vo.BusinessOpportunityVo;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class BusinessRecycleBinServiceImpl implements IBusinessRecycleBinService {

	@Autowired
	IBusinessRecycleBinMapper mapper;
	@Override
	public void recoverAll(String ids) {
		mapper.recoverAll(ids);
	}

	@Override
	public BusinessOpportunityVo findById(String id) {
		return mapper.findById(id);
	}

	@Override
	public List<BusinessOpportunityVo> findBusinessAll() {
		return mapper.findBusinessAll();
	}

	@Override
	public PageVo findPage(Integer pageNum, Integer pageSize,String CriteriaType,String content) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(mapper.pageList(beginIndex,endIndex,CriteriaType,content));
		pageVo.setTotalCount(mapper.totalCount(CriteriaType,content).intValue());
		return pageVo;
	}
}
