package com.zhidisoft.crm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.anno.ServiceLogAnno;
import com.zhidisoft.crm.entity.SysRoleFunc;
import com.zhidisoft.crm.mapper.ISysRoleFuncMapper;
import com.zhidisoft.crm.service.ISysRoleFuncService;
import com.zhidisoft.crm.util.UuidUtil;

@Service
@Transactional
public class SysRoleFuncServiceImpl implements ISysRoleFuncService {

	@Autowired
	ISysRoleFuncMapper roleFuncMapper;
	
	/**
	 * 给角色分配菜单(添加)
	 * @param roleId
	 * @param funcIds
	 */
	@Override
	@ServiceLogAnno("角色菜单-添加")
	public void save(String roleId, String[] funcIds) {
		//1. 删除已有的
		roleFuncMapper.deleteById(roleId);
		for (String funcId : funcIds) {
			SysRoleFunc roleFunc = new SysRoleFunc();
			roleFunc.setId(UuidUtil.uuid());
			roleFunc.setRole_id(roleId);
			roleFunc.setFunc_id(funcId);
			//2. 添加新的
			roleFuncMapper.save(roleFunc);
		}
	}

}
