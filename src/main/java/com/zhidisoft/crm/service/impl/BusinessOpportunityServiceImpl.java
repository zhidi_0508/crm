package com.zhidisoft.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.entity.BusinessBoost;
import com.zhidisoft.crm.entity.BusinessOpportunity;
import com.zhidisoft.crm.mapper.IBusinessBoostMapper;
import com.zhidisoft.crm.mapper.IBusinessOpportunityMapper;
import com.zhidisoft.crm.service.IBusinessOpportunityService;
import com.zhidisoft.crm.vo.BusinessCountVo;
import com.zhidisoft.crm.vo.BusinessOpportunityVo;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class BusinessOpportunityServiceImpl implements IBusinessOpportunityService{

	@Autowired
	IBusinessOpportunityMapper mapper;
	
	@Autowired
	IBusinessBoostMapper boostMapper;
	
	/**
	 * 添加商机
	 */
	@Override
	public int save(BusinessOpportunity busi) {
		return mapper.add(busi);
	}
	
	/**
	 * 批量删除
	 */
	@Override
	public void deleteAll(String ids) {
		mapper.deleteAll(ids);
	}
	
	/**
	 * 修改商机信息
	 */
	@Override
	public int updateById(BusinessOpportunity business) {
		return mapper.update(business);
	}
	
	/**
	 * 商机推进产品
	 * TODO
	 */
	@Override
	public boolean updateBoostById(BusinessOpportunity business) {
		mapper.updateBoostById(business);
		BusinessBoost boost = new BusinessBoost();
		boost.setBUSINESS_STATUS_ID(business.getBusiness_status_id());
		boost.setNEXT_CONTACT_TIME(business.getNext_contact_time());
		boost.setNEXT_CONTACT_INFO(business.getNext_contact_info().getBytes());
		boost.setDESCRIPTION(business.getRemark());
		boost.setUSER_ID(business.getCharge_user_id());
		boostMapper.add(boost);
		return true;
	}
	
	/**
	 * 根据id获取一条数据
	 */
	@Override
	public BusinessOpportunity findByBusiness(String id) {
		return mapper.findByBusiness(id);
	}
	
	/**
	 * 根据id获取商机(详情)
	 */
	@Override
	public BusinessOpportunityVo findById(String id) {
		return mapper.findById(id);
	}
	
	/**
	 * 获取所有商机
	 */
	@Override
	public List<BusinessOpportunityVo> findBusinessAll() {
		return mapper.findBusinessAll();
	}
	
	/**
	 * 商机总条数
	 */
	@Override
	public Integer BusinessCount() {
		return mapper.BusinessCount();
	}
	
	/**
	 * 高级分页列表查询
	 */
	@Override
	public PageVo findPage(Integer pageNum, Integer pageSize,String CriteriaType,String content) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(mapper.pageList(beginIndex,endIndex,CriteriaType,content));
		pageVo.setTotalCount(mapper.totalCount(CriteriaType,content).intValue());
		return pageVo;
	}
	
	/**
	 * 获取商机统计日期
	 */
	@Override
	public List<BusinessCountVo> countDay(String firstDay, String lastDay) {
		List<BusinessCountVo> list=mapper.countMonth(firstDay, lastDay);
		return list;
	}
}
