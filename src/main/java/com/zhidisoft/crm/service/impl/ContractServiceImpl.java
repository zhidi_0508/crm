package com.zhidisoft.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.entity.Contract;
import com.zhidisoft.crm.entity.Contract_status;
import com.zhidisoft.crm.mapper.IContractMapper;
import com.zhidisoft.crm.service.IContractService;
import com.zhidisoft.crm.vo.ContractCountVo;
import com.zhidisoft.crm.vo.ContractListVo;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class ContractServiceImpl implements IContractService {

	@Autowired
	IContractMapper mapper;
	
	/**
	 * 添加合同
	 */
	@Override
	public int save(Contract contract) {
		return mapper.save(contract);
	}
	
	/**
	 * 批量删除合同
	 */
	@Override
	public void deleteAll(String ids) {
		mapper.deleteAll(ids);
	}

	/**
	 * 修改合同
	 */
	@Override
	public int update(Contract contract) {
		return mapper.update(contract);
	}
	
	/**
	 * 查看合同详情
	 */
	@Override
	public ContractListVo findByid(String id) {
		return mapper.findByid(id);
	}

	/**
	 * 根据ID查询合同(数据库)
	 */
	@Override
	public Contract findByContract(String id) {
		return mapper.findByContract(id);
	}
	
	/**
	 * 合同列表分页查询
	 */
	@Override
	public PageVo findPage(Integer pageNum, Integer pageSize,String CriteriaType,String content) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(mapper.pageListWhere(beginIndex,endIndex,CriteriaType,content));
		pageVo.setTotalCount(mapper.totalCount(CriteriaType,content).intValue());
		return pageVo;
	}

	/**
	 * 查询所有合同状态
	 */
	@Override
	public List<Contract_status> findStatusAll() {
		return mapper.findStatusAll();
	}
	
	/**
	 * 查询合同日期统计图
	 */
	@Override
	public List<ContractCountVo> contractCount(String firstTime, String lastTime) {
		return mapper.contractCount(firstTime, lastTime);
	}

	/**
	 * 查询合同总条数
	 */
	@Override
	public Integer ContractCount() {
		return mapper.ContractCount();
	}
}
