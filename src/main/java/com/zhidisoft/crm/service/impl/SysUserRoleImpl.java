package com.zhidisoft.crm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.anno.ServiceLogAnno;
import com.zhidisoft.crm.entity.SysUserRole;
import com.zhidisoft.crm.mapper.ISysUserRoleMapper;
import com.zhidisoft.crm.service.ISysUserRoleService;
import com.zhidisoft.crm.util.UuidUtil;

@Service
@Transactional
public class SysUserRoleImpl implements ISysUserRoleService{
		@Autowired
		ISysUserRoleMapper userrole;

		/**
		 * 给用户分配角色
		 */
		@Override
		@ServiceLogAnno("用户角色-添加")
		public void saveUserRole(SysUserRole sysUserRole) {
			userrole.deleteById(sysUserRole.getUser_id());
			sysUserRole.setId(UuidUtil.uuid());
			userrole.save(sysUserRole);
		}

		/**
		 * 查询用户角色
		 */
		@Override
		@ServiceLogAnno("查询用户角色")
		public SysUserRole findById(String userId) {
			return userrole.findById(userId);
		}
		
}
