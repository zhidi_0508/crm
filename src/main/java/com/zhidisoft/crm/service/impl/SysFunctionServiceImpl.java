package com.zhidisoft.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.anno.ServiceLogAnno;
import com.zhidisoft.crm.entity.SysFunction;
import com.zhidisoft.crm.mapper.ISysFunctionMapper;
import com.zhidisoft.crm.service.ISysFunctionService;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class SysFunctionServiceImpl implements ISysFunctionService {

	@Autowired
	ISysFunctionMapper mapper;
	@Override
	@ServiceLogAnno("菜单-添加")
	public int save(SysFunction sysFunction) {
		return mapper.save(sysFunction);
	}

	@Override
	@ServiceLogAnno("菜单-删除")
	public int deleteById(String id) {
		return mapper.deleteById(id);
	}

	
	@Override
	@ServiceLogAnno("菜单-分页查询")
	public PageVo findPage(Integer pageNum, Integer pageSize) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		//pageVo.setList(mapper.pageList((pageNum - 1) * pageSize, pageSize));
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(mapper.pageList(beginIndex,endIndex));
		pageVo.setTotalCount(mapper.totalCount().intValue());
		return pageVo;
	}

	@Override
	public SysFunction findById(String id) {
		return mapper.findById(id);
	}
	@Override
	@ServiceLogAnno("菜单-编辑")
	public int update(SysFunction func) {
		return mapper.update(func);
	}

	@Override
	public SysFunction findByFunc(String func_name) {
		return mapper.findByFunc(func_name);
	}

	@Override
	public SysFunction findByParent(String parent_id) {
		return mapper.findByParent(parent_id);
	}

	@Override
	public List<SysFunction> findFuncAll() {
		return mapper.findFuncAll();
	}

	@Override
	public List<SysFunction> findByUser(String userId) {
		return mapper.findByUser(userId);
	}

}
