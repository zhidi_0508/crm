package com.zhidisoft.crm.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.anno.ServiceLogAnno;
import com.zhidisoft.crm.entity.SysUser;
import com.zhidisoft.crm.mapper.ISysUserMapper;
import com.zhidisoft.crm.service.ISysUserService;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class SysUserImpl implements ISysUserService {

	@Autowired
	ISysUserMapper mapper;
	
	/**
	 * 分页查询
	 */
	@Override
	public PageVo findPage(Integer pageNum, Integer pageSize) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		//mysql查询分页
		//pageVo.setList(mapper.pageList((pageNum - 1) * pageSize, pageSize));
		
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(mapper.pageList(beginIndex,endIndex));
		pageVo.setTotalCount(mapper.totalCount().intValue());
		return pageVo;
	}
	
	@Override
	public PageVo findPage(Integer pageNum, Integer pageSize, String username) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(mapper.pageListWhere(beginIndex,endIndex,username));
		pageVo.setTotalCount(mapper.totalCountWhere(username).intValue());
		return pageVo;
	}

	/**
	 * 根据ID查询用户
	 */
	@Override
	public SysUser findById(String userId) {
		return mapper.findById(userId);
	}

	/**
	 * 根据姓名查询用户
	 */
	@Override
	public SysUser findByUserName(String username) {
		return mapper.findByUserName(username);
	}

	/**
	 * 添加
	 */
	@Override
	@ServiceLogAnno("用户-添加")
	public int save(SysUser sysUser) {
		return mapper.save(sysUser);
	}

	/**
	 * 删除
	 */
	@Override
	@ServiceLogAnno("用户-删除")
	public int deleteById(String id) {
		return mapper.deleteById(id);
	}

	/**
	 * 编辑
	 * @return 
	 * @return 
	 */
	@Override
	@ServiceLogAnno("用户-编辑")
	public int update(SysUser sysUser) {
		return mapper.update(sysUser);
	}

	@Override
	public List<SysUser> findUserAll() {
		return mapper.findUserAll();
	}

	@Override
	public SysUser getUser(String charge_username) {
		return mapper.getUser(charge_username);
	}

}
