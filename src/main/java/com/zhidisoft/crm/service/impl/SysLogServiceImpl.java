package com.zhidisoft.crm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.mapper.ISysLogMapper;
import com.zhidisoft.crm.service.ISysLogService;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class SysLogServiceImpl implements ISysLogService {

	@Autowired
	ISysLogMapper mapper;
	
	@Override
	public PageVo findPage(Integer pageNum, Integer pageSize) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		pageVo.setList(mapper.pageList((pageNum - 1) * pageSize, pageSize));
		pageVo.setTotalCount(mapper.totalCount().intValue());
		return pageVo;
	}
}
