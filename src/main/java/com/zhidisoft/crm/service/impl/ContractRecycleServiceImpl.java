package com.zhidisoft.crm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.mapper.IContractRecycleBinMapper;
import com.zhidisoft.crm.service.IContractRecycleService;
import com.zhidisoft.crm.vo.ContractListVo;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class ContractRecycleServiceImpl implements IContractRecycleService {

	@Autowired
	IContractRecycleBinMapper mapper;
	
	@Override
	public PageVo findPage(Integer pageNum, Integer pageSize,String CriteriaType,String content) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(mapper.pageList(beginIndex,endIndex,CriteriaType,content));
		pageVo.setTotalCount(mapper.totalCount(CriteriaType,content).intValue());
		return pageVo;
	}

	@Override
	public ContractListVo findByid(String id) {
		return mapper.findByid(id);
	}

	@Override
	public void recoverAll(String ids) {
		mapper.recoverAll(ids);
	}
}
