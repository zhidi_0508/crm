package com.zhidisoft.crm.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.zhidisoft.crm.entity.BusinessBoost;
import com.zhidisoft.crm.mapper.IBusinessBoostMapper;
import com.zhidisoft.crm.service.IBusinessBoostService;
import com.zhidisoft.crm.vo.BusinessBoostListVo;
import com.zhidisoft.crm.vo.PageVo;

@Service
@Transactional
public class BusinessBoostServiceImpl implements IBusinessBoostService {

	@Autowired
	IBusinessBoostMapper mapper;
	
	/**
	 * 添加推进历史
	 */
	@Override
	public int add(BusinessBoost boost) {
		return mapper.add(boost);
	}

	/**
	 * 推进历史列表
	 */
	@Override
	public PageVo findPage(Integer pageNum, Integer pageSize) {
		PageVo pageVo = new PageVo();
		pageVo.setPageNum(pageNum);
		pageVo.setPageSize(pageSize);
		int beginIndex = (pageNum - 1) * pageSize;
		int endIndex = beginIndex + pageSize;
		pageVo.setList(mapper.pageList(beginIndex,endIndex));
		pageVo.setTotalCount(mapper.totalCount().intValue());
		return pageVo;
	}

	@Override
	public BusinessBoostListVo findById(String id) {
		return mapper.findById(id);
	}

}
