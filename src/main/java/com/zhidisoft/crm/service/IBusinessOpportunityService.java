package com.zhidisoft.crm.service;


import java.util.List;

import com.zhidisoft.crm.entity.BusinessOpportunity;
import com.zhidisoft.crm.vo.BusinessCountVo;
import com.zhidisoft.crm.vo.BusinessOpportunityVo;
import com.zhidisoft.crm.vo.PageVo;

public interface IBusinessOpportunityService {

	int save(BusinessOpportunity busi);
	
	void deleteAll(String ids);
	
	int updateById(BusinessOpportunity business);
	
	boolean updateBoostById(BusinessOpportunity business);
	
	BusinessOpportunityVo findById(String id);
	
	BusinessOpportunity findByBusiness(String id);

	PageVo findPage(Integer pageNum, Integer pageSize,String CriteriaType,String content);

	List<BusinessOpportunityVo> findBusinessAll();
	
	Integer BusinessCount();

	List<BusinessCountVo> countDay(String firstDay, String lastDay);
}
