package com.zhidisoft.crm.anno;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/**
 * service操作日志
 * @author MySpace
 *
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })	//设置作用范围
@Retention(RetentionPolicy.RUNTIME)						//设置运行时间
@Documented
public @interface ServiceLogAnno {

	String value() default "";
}
