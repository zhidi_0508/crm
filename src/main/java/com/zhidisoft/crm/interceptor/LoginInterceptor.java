package com.zhidisoft.crm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String servletPath = request.getServletPath();	//访问路径
		String userId = (String) request.getSession().getAttribute("userId");
		if (StringUtils.isEmpty(userId)) {
			String ctxPath = request.getContextPath();	//项目路径(如果项目部署在tomcat ROOT目录下,就是/)
			response.sendRedirect(ctxPath + "/login?servletPath="+servletPath);
			return false;
		}
		return super.preHandle(request, response, handler);
	}
}
