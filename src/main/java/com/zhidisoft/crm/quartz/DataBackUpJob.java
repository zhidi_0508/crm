package com.zhidisoft.crm.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.zhidisoft.crm.util.DataUtil;

public class DataBackUpJob extends QuartzJobBean {

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		System.out.println("备份结果:"+DataUtil.dataBackUp());
	}
}
