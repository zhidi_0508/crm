package com.zhidisoft.crm.quartz;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
/**
 * 发送邮件
 * @author MySpace
 *
 */
public class EmailJob {

	@Autowired
	JavaMailSender mailSender;
	
	public String email() throws MessagingException{
		// 建立邮件消息
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
		// 设置信息
		messageHelper.setTo("2019852822@qq.com");// 接受email(xxx:改为接收邮件的QQ邮箱)
		messageHelper.setFrom("313799008@qq.com");// 发送email(xxx:改为发送邮件的QQ邮箱)
		messageHelper.setSubject("我是标题"); // 标题
		messageHelper.setText("<html>你好,这是发送邮件的测试类</html>", true);
		mailSender.send(mimeMessage);
		return "redirect:list";
	}
}
