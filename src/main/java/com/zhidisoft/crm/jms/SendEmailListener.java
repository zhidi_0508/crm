package com.zhidisoft.crm.jms;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class SendEmailListener implements MessageListener {

	@Autowired
	JavaMailSender mailSender;

	@Override
	public void onMessage(Message message) {
		MapMessage map = (MapMessage) message;
		
		// 建立邮件消息
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
		try {
			String email = map.getString("email");
			String htmlResult = map.getString("htmlResult");
			// 建立邮件消息
			// 设置信息
			messageHelper.setTo(email);// 接受email
			messageHelper.setFrom("2552639726@qq.com");// 发送email
			messageHelper.setSubject("测试消息验证"); // 标题
			messageHelper.setText(htmlResult, true);
			mailSender.send(mimeMessage);
			System.out.println("发送成功");
		} catch (JMSException jmsE) {
			jmsE.printStackTrace();
		} catch (MessagingException msgE) {
			msgE.printStackTrace();
		}
		

	}
}
