package com.zhidisoft.crm.vo;

import java.util.Date;

/**
 * vo与entity的区别： 1，不需要序列化 2，作用： 与前端页面做交互，一一对应input
 * 
 * @author admin
 *
 */
public class SysUserVo {
	private String id;
	private String username;
	private String password;
	private Date create_time;
	private Integer is_admin;
	private String role_name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreate_time() {
		return create_time;
	}

	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}

	public Integer getIs_admin() {
		return is_admin;
	}

	public void setIs_admin(Integer is_admin) {
		this.is_admin = is_admin;
	}

	public String getRole_name() {
		return role_name;
	}

	public void setRole_name(String role_name) {
		this.role_name = role_name;
	}

	@Override
	public String toString() {
		return "SysUserVo [id=" + id + ", username=" + username + ", password=" + password + ", create_time="
				+ create_time + ", is_admin=" + is_admin + ", role_name=" + role_name + "]";
	}

}
