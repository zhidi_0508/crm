package com.zhidisoft.crm.vo;

import org.hibernate.validator.constraints.NotEmpty;

public class BusinessOpportunityFormVo {

	private String id;// id
	@NotEmpty(message="负责人不能为空")
	private String charge_username;// 负责人
	@NotEmpty(message="客户不能为空")
	private String customer_id;// 客户id
	private Integer business_money;// 商机金额
	@NotEmpty(message="商机名不能为空")
	private String business_name;// 商机名称
	private String contact_name;// 联系人
	private String province;// 合同签订地址省份
	private String city;// 合同签订地址城市
	private String area;// 合同签订地址地区
	private String areac;// 合同签订地址街道
	private String business_type_id;// 商机类型
	private String business_status_id;// 状态
	private String business_source_id;// 商机来源
	private Integer win_rate;// 赢单率
	private Integer expected_transaction_price;// 预计成交价
	private String next_contact_time;// 下次联系时间
	private String next_contact_info;// 下次联系内容
	private String remark;// 备注
	private String create_time;// 创建时间
	private String create_user_id;// 创建人
	private String contract_address;
	
	public String getContract_address() {
		return contract_address;
	}

	public void setContract_address(String contract_address) {
		this.contract_address = contract_address;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCharge_username() {
		return charge_username;
	}

	public void setCharge_username(String charge_username) {
		this.charge_username = charge_username;
	}

	public String getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(String customer_id) {
		this.customer_id = customer_id;
	}

	public Integer getBusiness_money() {
		return business_money;
	}

	public void setBusiness_money(Integer business_money) {
		this.business_money = business_money;
	}

	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public String getContact_name() {
		return contact_name;
	}

	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getAreac() {
		return areac;
	}

	public void setAreac(String areac) {
		this.areac = areac;
	}

	public String getBusiness_type_id() {
		return business_type_id;
	}

	public void setBusiness_type_id(String business_type_id) {
		this.business_type_id = business_type_id;
	}

	public String getBusiness_status_id() {
		return business_status_id;
	}

	public void setBusiness_status_id(String business_status_id) {
		this.business_status_id = business_status_id;
	}

	public String getBusiness_source_id() {
		return business_source_id;
	}

	public void setBusiness_source_id(String business_source_id) {
		this.business_source_id = business_source_id;
	}

	public Integer getWin_rate() {
		return win_rate;
	}

	public void setWin_rate(Integer win_rate) {
		this.win_rate = win_rate;
	}

	public Integer getExpected_transaction_price() {
		return expected_transaction_price;
	}

	public void setExpected_transaction_price(Integer expected_transaction_price) {
		this.expected_transaction_price = expected_transaction_price;
	}

	public String getNext_contact_time() {
		return next_contact_time;
	}

	public void setNext_contact_time(String next_contact_time) {
		this.next_contact_time = next_contact_time;
	}

	public String getNext_contact_info() {
		return next_contact_info;
	}

	public void setNext_contact_info(String next_contact_info) {
		this.next_contact_info = next_contact_info;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getCreate_user_id() {
		return create_user_id;
	}

	public void setCreate_user_id(String create_user_id) {
		this.create_user_id = create_user_id;
	}

}
