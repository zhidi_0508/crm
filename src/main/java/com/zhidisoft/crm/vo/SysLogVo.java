package com.zhidisoft.crm.vo;

import java.util.Date;

public class SysLogVo {

	private String id;
	private String method_info;
	private String username;
	private Date create_time;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getMethod_info() {
		return method_info;
	}
	public void setMethod_info(String method_info) {
		this.method_info = method_info;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	
}
