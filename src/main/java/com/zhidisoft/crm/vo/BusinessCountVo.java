package com.zhidisoft.crm.vo;

import java.util.Date;

import com.zhidisoft.crm.util.DateUtil;

public class BusinessCountVo {

	private Date createTime;
	@SuppressWarnings("unused")
	private String _createTime;
	private Integer numCount;
	
	public String get_createTime() {
		return DateUtil.parseStr(this.createTime, "yyyy年MM月dd日"); 
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getNumCount() {
		return numCount;
	}

	public void setNumCount(Integer numCount) {
		this.numCount = numCount;
	}
	
}
