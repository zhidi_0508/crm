package com.zhidisoft.crm.vo;

import org.hibernate.validator.constraints.NotEmpty;

public class BusinessBoostVo{
	private String ID;
	@NotEmpty(message = "合同阶段未填写!")
	private String BUSINESS_STATUS_ID;
	@NotEmpty(message = "联系时间未填写!")
	private String NEXT_CONTACT_TIME;
	@NotEmpty(message = "联系内容未填写!")
	private String NEXT_CONTACT_INFO;
	@NotEmpty(message = "阶段描述未填写!")
	private String DESCRIPTION;
	
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getBUSINESS_STATUS_ID() {
		return BUSINESS_STATUS_ID;
	}
	public void setBUSINESS_STATUS_ID(String bUSINESS_STATUS_ID) {
		BUSINESS_STATUS_ID = bUSINESS_STATUS_ID;
	}
	public String getNEXT_CONTACT_TIME() {
		return NEXT_CONTACT_TIME;
	}
	public void setNEXT_CONTACT_TIME(String nEXT_CONTACT_TIME) {
		NEXT_CONTACT_TIME = nEXT_CONTACT_TIME;
	}
	public String getNEXT_CONTACT_INFO() {
		return NEXT_CONTACT_INFO;
	}
	public void setNEXT_CONTACT_INFO(String nEXT_CONTACT_INFO) {
		NEXT_CONTACT_INFO = nEXT_CONTACT_INFO;
	}
	public String getDESCRIPTION() {
		return DESCRIPTION;
	}
	public void setDESCRIPTION(String dESCRIPTION) {
		DESCRIPTION = dESCRIPTION;
	}
}
