package com.zhidisoft.crm.vo;

import java.util.Date;

public class BusinessBoostListVo {
	private String id;
	private String status_name;
	private Date next_contact_time;
	private byte[] next_contact_info;
	private byte[] description;
	private String username;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getStatus_name() {
		return status_name;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public Date getNext_contact_time() {
		return next_contact_time;
	}

	public void setNext_contact_time(Date next_contact_time) {
		this.next_contact_time = next_contact_time;
	}

	public byte[] getNext_contact_info() {
		return next_contact_info;
	}

	public void setNext_contact_info(byte[] next_contact_info) {
		this.next_contact_info = next_contact_info;
	}

	public byte[] getDescription() {
		return description;
	}

	public void setDescription(byte[] description) {
		this.description = description;
	}
}
