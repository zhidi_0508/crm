package com.zhidisoft.crm.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ContractListVo {
	private String id;
	private String contract_no; // 合同编号
	private String customer_name; // 客户
	private String contact_name; // 联系人
	private String username; // 负责人
	private Date date_of_contract;// 签约日期
	private Integer contract_money; // 合同金额
	private String status_name;// 合同状态
	private Date contract_expiry_date;// 合同到期日期
	@SuppressWarnings("unused")
	private Integer end_time;// 距合同到期天数
	private String business_name ;//来源商机
	private Date effective_date_of_contract;//合同生效日期
	private byte[] terms_and_conditions;//条件条款
	private String the_contract_description;//合同描述
	
	
	public String getBusiness_name() {
		return business_name;
	}

	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}

	public Date getEffective_date_of_contract() {
		return effective_date_of_contract;
	}

	public void setEffective_date_of_contract(Date effective_date_of_contract) {
		this.effective_date_of_contract = effective_date_of_contract;
	}

	public byte[] getTerms_and_conditions() {
		return terms_and_conditions;
	}

	public void setTerms_and_conditions(byte[] terms_and_conditions) {
		this.terms_and_conditions = terms_and_conditions;
	}

	public String getThe_contract_description() {
		return the_contract_description;
	}

	public void setThe_contract_description(String the_contract_description) {
		this.the_contract_description = the_contract_description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContract_no() {
		return contract_no;
	}

	public void setContract_no(String contract_no) {
		this.contract_no = contract_no;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getContact_name() {
		return contact_name;
	}

	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getDate_of_contract() {
		return date_of_contract;
	}

	public void setDate_of_contract(Date date_of_contract) {
		this.date_of_contract = date_of_contract;
	}

	public Integer getContract_money() {
		return contract_money;
	}

	public void setContract_money(Integer contract_money) {
		this.contract_money = contract_money;
	}

	public String getStatus_name() {
		return status_name;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public Date getContract_expiry_date() {
		return contract_expiry_date;
	}

	public void setContract_expiry_date(Date contract_expiry_date) {
		this.contract_expiry_date = contract_expiry_date;
	}

	@SuppressWarnings("deprecation")
	public Integer getEnd_time() {
		String time = new SimpleDateFormat("yyyy-MM-dd").format(contract_expiry_date);
		String taskDate = new SimpleDateFormat("yyyy-MM-dd").format(date_of_contract);
		long endTime = 0;
		long beginTime = 0;
		if (time != null || !"".equals(time)) {
			String year = time.substring(0, 4);
			String month = time.substring(5, 7);
			String date = time.substring(8, 10);
			beginTime = new Date(Integer.parseInt(year), Integer.parseInt(month), Integer.parseInt(date)).getTime();
		} else {
			time = "0";
		}
		if (taskDate != null || !"".equals(taskDate)) {
			String y = taskDate.substring(0, 4);
			String m = taskDate.substring(5, 7);
			String d = taskDate.substring(8, 10);
			endTime = new Date(Integer.parseInt(y), Integer.parseInt(m), Integer.parseInt(d)).getTime();
		} else {
			taskDate = "0";
		}
		int end_time = (int) ((beginTime - endTime) / 1000 / 60 / 60 / 24);
		if (end_time < 0) {
			end_time = 0;
		}
		return end_time;
	}

	public void setEnd_time(Integer end_time) {
		this.end_time = end_time;
	}
}
