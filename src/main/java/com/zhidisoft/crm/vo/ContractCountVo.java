package com.zhidisoft.crm.vo;

import java.util.Date;

import com.zhidisoft.crm.util.DateUtil;

public class ContractCountVo {

	private Date dateOfContract;
	@SuppressWarnings("unused")
	private String _dateOfContract;
	private Integer numCount;
	
	public Date getDateOfContract() {
		return dateOfContract;
	}

	public void setDateOfContract(Date dateOfContract) {
		this.dateOfContract = dateOfContract;
	}

	public String get_dateOfContract() {
		return DateUtil.parseStr(this.dateOfContract, "yyyy年MM月dd日");
	}

	public Integer getNumCount() {
		return numCount;
	}

	public void setNumCount(Integer numCount) {
		this.numCount = numCount;
	}
}
