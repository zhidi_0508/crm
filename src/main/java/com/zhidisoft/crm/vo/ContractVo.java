package com.zhidisoft.crm.vo;

import org.hibernate.validator.constraints.NotEmpty;

public class ContractVo {
	private String id;
	@NotEmpty(message = "合同编号未填写!")
	private String CONTRACT_NO;	//合同编号
	private String CUSTOMER_ID; //来源客户
	@NotEmpty(message = "签约日期未填写!")
	private String DATE_OF_CONTRACT;	//签约日期
	private String BUSINESS_OPPORTUNITY_ID;	//来源商机
	private String CONTRACT_STATUS_ID;
	@NotEmpty(message = "联系人未填写!")
	private String CONTACT_NAME;	//联系人
	@NotEmpty(message = "负责人未填写!")
	private String CHARGE_USER_ID;	//负责人
	private Integer CONTRACT_MONEY;	//合同金额
	@NotEmpty(message = "合同生效日期未填写!")
	private String EFFECTIVE_DATE_OF_CONTRACT;	//合同生效日期
	@NotEmpty(message = "合同到期日期未填写!")
	private String CONTRACT_EXPIRY_DATE;	//合同到期日期
	@NotEmpty(message = "条件和条款未填写!")
	private String TERMS_AND_CONDITIONS;	//条件和条款
	private String THE_CONTRACT_DESCRIPTION;	//合同描述(不多于150字)
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCONTRACT_STATUS_ID() {
		return CONTRACT_STATUS_ID;
	}
	public void setCONTRACT_STATUS_ID(String cONTRACT_STATUS_ID) {
		CONTRACT_STATUS_ID = cONTRACT_STATUS_ID;
	}
	public String getCONTRACT_NO() {
		return CONTRACT_NO;
	}
	public void setCONTRACT_NO(String cONTRACT_NO) {
		CONTRACT_NO = cONTRACT_NO;
	}
	public String getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}
	public void setCUSTOMER_ID(String cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}
	public String getDATE_OF_CONTRACT() {
		return DATE_OF_CONTRACT;
	}
	public void setDATE_OF_CONTRACT(String dATE_OF_CONTRACT) {
		DATE_OF_CONTRACT = dATE_OF_CONTRACT;
	}
	public String getBUSINESS_OPPORTUNITY_ID() {
		return BUSINESS_OPPORTUNITY_ID;
	}
	public void setBUSINESS_OPPORTUNITY_ID(String bUSINESS_OPPORTUNITY_ID) {
		BUSINESS_OPPORTUNITY_ID = bUSINESS_OPPORTUNITY_ID;
	}
	public String getCONTACT_NAME() {
		return CONTACT_NAME;
	}
	public void setCONTACT_NAME(String cONTACT_NAME) {
		CONTACT_NAME = cONTACT_NAME;
	}
	public String getCHARGE_USER_ID() {
		return CHARGE_USER_ID;
	}
	public void setCHARGE_USER_ID(String cHARGE_USER_ID) {
		CHARGE_USER_ID = cHARGE_USER_ID;
	}
	public Integer getCONTRACT_MONEY() {
		return CONTRACT_MONEY;
	}
	public void setCONTRACT_MONEY(Integer cONTRACT_MONEY) {
		CONTRACT_MONEY = cONTRACT_MONEY;
	}
	public String getEFFECTIVE_DATE_OF_CONTRACT() {
		return EFFECTIVE_DATE_OF_CONTRACT;
	}
	public void setEFFECTIVE_DATE_OF_CONTRACT(String eFFECTIVE_DATE_OF_CONTRACT) {
		EFFECTIVE_DATE_OF_CONTRACT = eFFECTIVE_DATE_OF_CONTRACT;
	}
	public String getCONTRACT_EXPIRY_DATE() {
		return CONTRACT_EXPIRY_DATE;
	}
	public void setCONTRACT_EXPIRY_DATE(String cONTRACT_EXPIRY_DATE) {
		CONTRACT_EXPIRY_DATE = cONTRACT_EXPIRY_DATE;
	}
	public String getTERMS_AND_CONDITIONS() {
		return TERMS_AND_CONDITIONS;
	}
	public void setTERMS_AND_CONDITIONS(String tERMS_AND_CONDITIONS) {
		TERMS_AND_CONDITIONS = tERMS_AND_CONDITIONS;
	}
	public String getTHE_CONTRACT_DESCRIPTION() {
		return THE_CONTRACT_DESCRIPTION;
	}
	public void setTHE_CONTRACT_DESCRIPTION(String tHE_CONTRACT_DESCRIPTION) {
		THE_CONTRACT_DESCRIPTION = tHE_CONTRACT_DESCRIPTION;
	}
}
