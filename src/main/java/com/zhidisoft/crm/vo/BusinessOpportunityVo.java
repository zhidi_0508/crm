package com.zhidisoft.crm.vo;

import java.util.Date;

public class BusinessOpportunityVo {

	private String id;
	private String customer_name;
	private String business_money;
	private String business_name;
	private String contact_name;
	private String contract_address;
	private String source_name;
	private String type_name;
	private String status_name;
	private String business_status_id;
	private String win_rate;
	private String expected_transaction_price;
	private Date next_contact_time;
	private byte[] remark;
	private String next_contact_info;
	private String charge_user_name;
	private String charge_user_id;
	private String create_user_name;
	private String create_user_id;
	private Date create_time;
	private Date update_time;
	
	
	public String getBusiness_status_id() {
		return business_status_id;
	}
	public void setBusiness_status_id(String business_status_id) {
		this.business_status_id = business_status_id;
	}
	public String getCharge_user_id() {
		return charge_user_id;
	}
	public void setCharge_user_id(String charge_user_id) {
		this.charge_user_id = charge_user_id;
	}
	public String getCreate_user_id() {
		return create_user_id;
	}
	public void setCreate_user_id(String create_user_id) {
		this.create_user_id = create_user_id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getBusiness_money() {
		return business_money;
	}
	public void setBusiness_money(String business_money) {
		this.business_money = business_money;
	}
	public String getBusiness_name() {
		return business_name;
	}
	public void setBusiness_name(String business_name) {
		this.business_name = business_name;
	}
	public String getContact_name() {
		return contact_name;
	}
	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}
	public String getContract_address() {
		return contract_address;
	}
	public void setContract_address(String contract_address) {
		this.contract_address = contract_address;
	}
	public String getSource_name() {
		return source_name;
	}
	public void setSource_name(String source_name) {
		this.source_name = source_name;
	}
	public String getType_name() {
		return type_name;
	}
	public void setType_name(String type_name) {
		this.type_name = type_name;
	}
	public String getStatus_name() {
		return status_name;
	}
	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}
	public String getWin_rate() {
		return win_rate;
	}
	public void setWin_rate(String win_rate) {
		this.win_rate = win_rate;
	}
	public String getExpected_transaction_price() {
		return expected_transaction_price;
	}
	public void setExpected_transaction_price(String expected_transaction_price) {
		this.expected_transaction_price = expected_transaction_price;
	}
	public String getNext_contact_info() {
		return next_contact_info;
	}
	public void setNext_contact_info(String next_contact_info) {
		this.next_contact_info = next_contact_info;
	}
	public String getCharge_user_name() {
		return charge_user_name;
	}
	public void setCharge_user_name(String charge_user_name) {
		this.charge_user_name = charge_user_name;
	}
	public String getCreate_user_name() {
		return create_user_name;
	}
	public void setCreate_user_name(String create_user_name) {
		this.create_user_name = create_user_name;
	}
	public Date getNext_contact_time() {
		return next_contact_time;
	}
	public void setNext_contact_time(Date next_contact_time) {
		this.next_contact_time = next_contact_time;
	}
	public byte[] getRemark() {
		return remark;
	}
	public void setRemark(byte[] remark) {
		this.remark = remark;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public Date getUpdate_time() {
		return update_time;
	}
	public void setUpdate_time(Date update_time) {
		this.update_time = update_time;
	}
	
}
