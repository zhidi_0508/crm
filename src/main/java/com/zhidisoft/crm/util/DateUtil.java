package com.zhidisoft.crm.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.springframework.util.StringUtils;
/**
 * 日期工具类
 * @author MySpace
 *
 */
public class DateUtil {

	/**
	 * 日期格式转字符串格式
	 * @param date
	 * @param format
	 * @return
	 */
	public static String parseStr(Date date, String format) {
		if (StringUtils.isEmpty(format)) {
			format = "yyyy-MM-dd";
		}
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}
	
	/**
	 * 字符串格式转日期格式
	 * @param dateStr
	 * @return
	 */
	public static Date toDate(String dateStr) {
		try {
			return DateFormat.getDateInstance().parse(dateStr);
		} catch (ParseException e) {
			return new Date();
		}

	}

	/**
	 * 获取本周第一天
	 * @return
	 */
	public static LocalDate getFirstDayOfWeek() {
		Instant instant = Instant.now();
		OffsetDateTime instant1 = instant.atOffset(ZoneOffset.ofHours(8));
		// instant.atZone(ZoneId.)
		// 获取今天是本周的第几天
		int dayOfWeek = instant1.getDayOfWeek().getValue();
		return instant1.minusDays(dayOfWeek - 1).toLocalDate();
	}

	/**
	 * 获取本周最后一天
	 * @return
	 */
	public static LocalDate getLastDayOfWeek() {
		Instant instant = Instant.now();
		OffsetDateTime instant1 = instant.atOffset(ZoneOffset.ofHours(8));
		// 获取今天是本周的第几天
		int dayOfWeek = instant1.getDayOfWeek().getValue();
		// 获取本周剩余天数
		int tmp = 7 - dayOfWeek;
		return instant1.plusDays(tmp).toLocalDate();
	}
	
	/**
	 * 获取本月第一天
	 * @return
	 */
	public static LocalDate getFirstDayOfMonth() {
		Instant instant = Instant.now();
		OffsetDateTime instant1 = instant.atOffset(ZoneOffset.ofHours(8));
		// 获取今天是本月的第几天
		int dayOfMonth = instant1.getDayOfMonth();
		return instant1.minusDays(dayOfMonth - 1).toLocalDate();
	}
	
	/**
	 * 获取本月最后一天
	 * @return
	 */
	public static LocalDate getLastDayOfMonth() {
		Instant instant = Instant.now();
		OffsetDateTime instant1 = instant.atOffset(ZoneOffset.ofHours(8));
		// 获取今天是本月的第几天
		int dayOfMonth = instant1.getDayOfMonth();
		// 获取今天几月
		int month = instant1.getMonthValue();
		// 获取年数
		int year = instant1.getYear();
		int day = 31;
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			day = 30;
			break;
		case 2:
			if (year % 4 == 0 & year % 100 != 0 || year % 400 == 0) {
				day = 29;
			} else {
				day = 28;
			}
			default:			
		}
			
		// 获取本月剩余天数
		int tmp = day - dayOfMonth;
		return instant1.plusDays(tmp).toLocalDate();
	}
}