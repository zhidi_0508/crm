package com.zhidisoft.crm.util;

import java.util.UUID;
/**
 * uuid工具类
 * @author MySpace
 *
 */
public class UuidUtil {

	public static String uuid(){
		return UUID.randomUUID().toString().replace("-", "");
	}
	
}
